using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MouseClick : MonoBehaviour
{

  public string clickIdentifier = "";
  public string precinctTurnout = "";
  public string precinctCastedBallots = "";
  public string precinctMarginofWin = "";
  public string democratPercentage = "";
  public string republicanPercentage = "";


  public string YearSelector = "";
  public string whiteDemoString = "";
  public string blackDemoString = "";
  public string asianDemoString = "";
  public string hispanicDemoString = "";

  public string PVIString = "";

  public string popOfPrecinct = "";
  public string changeOfPopInPrecinctOverTime = "";

  public string specialText = "";

  private string demoIdentifier;

    public void Update()
    {
        {
            {

              /////////////////////////////////////////////////////////////////
              /// MAP SELECTION
              /////////////////////////////////////////////////////////////////



                if (Input.GetMouseButtonDown(0))
                {
                  Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                  RaycastHit2D hitData = Physics2D.Raycast(new Vector2(worldPosition.x, worldPosition.y), Vector2.zero, 0);

                  if (hitData.collider != null) //// If you hit something
                    {
                      clickIdentifier = hitData.collider.name; //// Sets the name of the sprite the mouse clicked to clickIdent for later
                      displayBasicInfo(); //// Using the clickIdent name, starts the display info function
                    }

                }
              }
            }
          }

     public void displayBasicInfo()
      {
        gameObject.GetComponent<UIManager>().dataTextBox.text = ""; /// Resets the data text box
        specialText = "";


        determineYear(); /// Sets all the basic info depending on the year
        setSpecialText();
        setNonSpecialText(); /// Sets the basic text
      }

     public void determineYear()
      {

        if(gameObject.GetComponent<UIManager>().yearOneString != null && gameObject.GetComponent<UIManager>().yearTwoString == null || gameObject.GetComponent<UIManager>().yearOneString == gameObject.GetComponent<UIManager>().yearTwoString || gameObject.GetComponent<UIManager>().yearOneString != null && gameObject.GetComponent<UIManager>().yearTwoString == "")
          {
            YearSelector = gameObject.GetComponent<UIManager>().yearOneString;

            Debug.Log("Year Selector is: " + YearSelector + ". While Year One is " + gameObject.GetComponent<UIManager>().yearOneString);
          }

        else if(gameObject.GetComponent<UIManager>().yearTwoString != null)
          {
            YearSelector = gameObject.GetComponent<UIManager>().yearTwoString;

            Debug.Log("Year Selector is: " + YearSelector + ". While Year Two is " + gameObject.GetComponent<UIManager>().yearTwoString);
          }

            if(YearSelector == "2012")
              {
              //  whiteDemoString = "" + (float)gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2012[clickIdentifier];
                //blackDemoString = "" + (float)gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2012[clickIdentifier];
              //  asianDemoString = "" + (float)gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2012[clickIdentifier];
              //  hispanicDemoString = "" + (float)gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2012[clickIdentifier];
              }

            if(YearSelector == "2016")
              {
                whiteDemoString = "" + (float)gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2016[clickIdentifier];
                blackDemoString = "" + (float)gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2016[clickIdentifier];
                asianDemoString = "" + (float)gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2016[clickIdentifier];
                hispanicDemoString = "" + (float)gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2016[clickIdentifier];

                if(gameObject.GetComponent<InitStartup>().democraticPVIfor2016.ContainsKey(clickIdentifier))
                  {
                    PVIString = "D+" + (float)gameObject.GetComponent<InitStartup>().democraticPVIfor2016[clickIdentifier];
                  }

                else if (gameObject.GetComponent<InitStartup>().republicanPVIfor2016.ContainsKey(clickIdentifier))
                  {
                    PVIString = "R+" + (float)gameObject.GetComponent<InitStartup>().republicanPVIfor2016[clickIdentifier];
                  }
              }

            if(YearSelector == "2018")
              {
                whiteDemoString = "" + (float)gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2018[clickIdentifier];
                blackDemoString = "" + (float)gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2018[clickIdentifier];
                asianDemoString = "" + (float)gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2018[clickIdentifier];
                hispanicDemoString = "" + (float)gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2018[clickIdentifier];

                if(gameObject.GetComponent<InitStartup>().democraticPVIfor2018.ContainsKey(clickIdentifier))
                  {
                    PVIString = "D+" + (float)gameObject.GetComponent<InitStartup>().democraticPVIfor2018[clickIdentifier];
                  }

                else if (gameObject.GetComponent<InitStartup>().republicanPVIfor2018.ContainsKey(clickIdentifier))
                  {
                    PVIString = "R+" + (float)gameObject.GetComponent<InitStartup>().republicanPVIfor2018[clickIdentifier];
                  }
              }

            if(YearSelector == "2020")
              {
                whiteDemoString = "" + (float)gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2020[clickIdentifier];
                blackDemoString = "" + (float)gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2020[clickIdentifier];
                asianDemoString = "" + (float)gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2020[clickIdentifier];
                hispanicDemoString = "" + (float)gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2020[clickIdentifier];

                if(gameObject.GetComponent<InitStartup>().democraticPVIfor2020.ContainsKey(clickIdentifier))
                  {
                    PVIString = "D+" + (float)gameObject.GetComponent<InitStartup>().democraticPVIfor2020[clickIdentifier];
                  }

                else if (gameObject.GetComponent<InitStartup>().republicanPVIfor2020.ContainsKey(clickIdentifier))
                  {
                    PVIString = "R+" + (float)gameObject.GetComponent<InitStartup>().republicanPVIfor2020[clickIdentifier];
                  }
              }

            if(YearSelector == "2021")
              {
                whiteDemoString = "" + (float)gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2021[clickIdentifier];
                blackDemoString = "" + (float)gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2021[clickIdentifier];
                asianDemoString = "" + (float)gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2021[clickIdentifier];
                hispanicDemoString = "" + (float)gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2021[clickIdentifier];

                if(gameObject.GetComponent<InitStartup>().democraticPVIfor2021.ContainsKey(clickIdentifier))
                  {
                    PVIString = "D+" + (float)gameObject.GetComponent<InitStartup>().democraticPVIfor2021[clickIdentifier];
                  }

                else if (gameObject.GetComponent<InitStartup>().republicanPVIfor2021.ContainsKey(clickIdentifier))
                  {
                    PVIString = "R+" + (float)gameObject.GetComponent<InitStartup>().republicanPVIfor2021[clickIdentifier];
                  }
              }
      }

      public void setNonSpecialText()
        {
          gameObject.GetComponent<UIManager>().dataTextBox.text = gameObject.GetComponent<UIManager>().dataTextBox.text + "Precinct: " + clickIdentifier + ", which has a PVI of " + PVIString + ".\n\n\nBASIC INFORMATION:\n\nWhite Population: " + whiteDemoString + "%.\n\nAfrican-American Population: " + blackDemoString + "%.\n\nAsian-American Population: " + asianDemoString + "%.\n\nHispanic-American Population: " + hispanicDemoString + "%." + "\n\n" + specialText;
        }


      /////////////////////////////////////////////////////////////////
      /// Actually displaying the Information
      /////////////////////////////////////////////////////////////////

      public void setSpecialText()
        {

          if(gameObject.GetComponent<UIManager>().specialFuncSelector == 2 && gameObject.GetComponent<UIManager>().specialFuncActivated == true)
           {

             if(gameObject.GetComponent<InitStartup>().changeInDemocraticPVI.ContainsKey(clickIdentifier))
              {
                specialText  = "This precincts PVI shifted " + (float)gameObject.GetComponent<InitStartup>().changeInDemocraticPVI[clickIdentifier] + " points towards the Democrats over these years.";
              }

              else if(gameObject.GetComponent<InitStartup>().changeInDemocraticPVINeg.ContainsKey(clickIdentifier))
               {
                 specialText  = "This precincts PVI shifted " + (float)gameObject.GetComponent<InitStartup>().changeInDemocraticPVINeg[clickIdentifier] + " points towards the Republicans over these years.";
               }

             else if(gameObject.GetComponent<InitStartup>().changeInRepublicanPVINeg.ContainsKey(clickIdentifier))
              {
                specialText  = "This precincts PVI shifted " + (float)gameObject.GetComponent<InitStartup>().changeInRepublicanPVINeg[clickIdentifier] + " points towards the Democrats over these years.";
              }

             else if(gameObject.GetComponent<InitStartup>().changeInRepublicanPVI.ContainsKey(clickIdentifier))
              {
                specialText  = "This precincts PVI shifted " + (float)gameObject.GetComponent<InitStartup>().changeInRepublicanPVI[clickIdentifier] + " points towards the Republicans over these years.";
              }

              else if(gameObject.GetComponent<InitStartup>().changeInPVIRepFlip.ContainsKey(clickIdentifier))
               {
                 specialText  = "This precincts PVI shifted " + (float)gameObject.GetComponent<InitStartup>().changeInPVIRepFlip[clickIdentifier] + " points towards the Republicans over these years.";
               }

             else if(gameObject.GetComponent<InitStartup>().changeInPVIDemFlip.ContainsKey(clickIdentifier))
              {
                specialText  = "This precincts PVI shifted " + (float)gameObject.GetComponent<InitStartup>().changeInPVIDemFlip[clickIdentifier] + " points towards the Democrats over these years.";
              }
           }


         if(gameObject.GetComponent<UIManager>().specialFuncSelector == 5 && gameObject.GetComponent<UIManager>().specialFuncActivated == true)
          {
            string changeInCastedBallString = "" + (float)gameObject.GetComponent<InitStartup>().changeInCastedBallots[clickIdentifier];

            string changeInCastedBallPercentageString = "" + (float)gameObject.GetComponent<InitStartup>().changeinCastedBalPercentageTable[clickIdentifier];

            specialText = "The number of casted ballots in this precinct changed by " + changeInCastedBallString + " over these years. This represents a change of " + changeInCastedBallPercentageString + "%.";
          }


          else if(gameObject.GetComponent<UIManager>().demoActivated == false && gameObject.GetComponent<UIManager>().specialFuncActivated == false && gameObject.GetComponent<UIManager>().doingTwoYears == true) /// Two Years
            {

              if(gameObject.GetComponent<InitStartup>().changeInMarginBoth.ContainsKey(clickIdentifier))
                {
                  precinctMarginofWin = "Over these years, both the Republicans and Democrats incresaed their margins.";
                }

              else if(gameObject.GetComponent<InitStartup>().changeInMarginRepublican.ContainsKey(clickIdentifier))
                {
                  precinctMarginofWin = "Over these years, Republicans increased their margin by " + (float)gameObject.GetComponent<InitStartup>().changeInMarginRepublican[clickIdentifier] + "%.";
                }

              else if(gameObject.GetComponent<InitStartup>().changeInMarginDemocratic.ContainsKey(clickIdentifier))
                {
                  precinctMarginofWin = "Over these years, Democrats increased their margin by " + (float)gameObject.GetComponent<InitStartup>().changeInMarginDemocratic[clickIdentifier] + "%.";
                }

                precinctTurnout = "" + (float)gameObject.GetComponent<InitStartup>().changeInTurnout[clickIdentifier];

                precinctCastedBallots = "" + (float)gameObject.GetComponent<InitStartup>().changeInCastedBallots[clickIdentifier];

                specialText = precinctMarginofWin + " The number of casted ballots changed by " + precinctCastedBallots + " total ballots.\n\nTurnout increased by " + precinctTurnout + "%.";
            }


          else if(gameObject.GetComponent<UIManager>().demoActivated == false && gameObject.GetComponent<UIManager>().specialFuncActivated == false && gameObject.GetComponent<UIManager>().doingOneYear == true) /// One Year
            {

              if(gameObject.GetComponent<InitStartup>().tiedPrecinct.ContainsKey(clickIdentifier)) //// If Tied
                {
                  precinctMarginofWin = "During this year, the Democrats and Republicans tied.";
                }

              if(gameObject.GetComponent<InitStartup>().marginOfWinDemocratWin.ContainsKey(clickIdentifier)) //// If Democrats Won
                {
                  precinctMarginofWin  = "During this year, Democrats won by a margin of " + (float)gameObject.GetComponent<InitStartup>().marginOfWinDemocratWin[clickIdentifier] + " votes";
                }


              if(gameObject.GetComponent<InitStartup>().marginOfWinRepublicanWin.ContainsKey(clickIdentifier)) //// If Republicans won
                {
                  precinctMarginofWin  = "During this year, Republicans won by a margin of " + (float)gameObject.GetComponent<InitStartup>().marginOfWinRepublicanWin[clickIdentifier] + " votes";
                }



              democratPercentage = "" + (float)gameObject.GetComponent<InitStartup>().percentageDemocratic[clickIdentifier];

              republicanPercentage = "" + (float)gameObject.GetComponent<InitStartup>().percentageRepublican[clickIdentifier];

              precinctTurnout = "" + (float)gameObject.GetComponent<InitStartup>().turnoutPercentage[clickIdentifier];

              precinctCastedBallots = "" + (float)gameObject.GetComponent<InitStartup>().castedBallots[clickIdentifier];

              specialText = precinctMarginofWin + " out of " + precinctCastedBallots + " total ballots.\n\nDemocrats got " + democratPercentage + "% while Republicans got " + republicanPercentage + "%.\n\nTurnout in this precinct was " + precinctTurnout + "%.";
            }




          else if(gameObject.GetComponent<UIManager>().demoActivated == true && gameObject.GetComponent<UIManager>().doingTwoYears == true)
            {

              string popChangeIdentifier = "";

              demoDeterminer();

              if(gameObject.GetComponent<InitStartup>().demoChangeOverTimePos.ContainsKey(clickIdentifier))
                {
                  changeOfPopInPrecinctOverTime = "" + (float)gameObject.GetComponent<InitStartup>().demoChangeOverTimePos[clickIdentifier];

                  popChangeIdentifier = " increased by " + changeOfPopInPrecinctOverTime;
                }

              else if(gameObject.GetComponent<InitStartup>().demoChangeOverTimeNeg.ContainsKey(clickIdentifier))
                {
                  changeOfPopInPrecinctOverTime = "" + (float)gameObject.GetComponent<InitStartup>().demoChangeOverTimeNeg[clickIdentifier];

                  popChangeIdentifier = " decreased by " + changeOfPopInPrecinctOverTime;
                }

              if(!gameObject.GetComponent<InitStartup>().demoChangeOverTimeNeg.ContainsKey(clickIdentifier) && !gameObject.GetComponent<InitStartup>().demoChangeOverTimePos.ContainsKey(clickIdentifier))
                {
                  specialText = "\n\n\nThis precinct doesn't have data for these two years. It might've been removed or combined into another one.\n\n";
                }

               else if (gameObject.GetComponent<InitStartup>().demoChangeOverTimeNeg.ContainsKey(clickIdentifier) || gameObject.GetComponent<InitStartup>().demoChangeOverTimePos.ContainsKey(clickIdentifier))
                {
                  specialText = "\n\n\nThis precincts population of " + demoIdentifier + popChangeIdentifier + "% over these years.\n\n";
                }
            }

          else if(gameObject.GetComponent<UIManager>().specialFuncSelector == 3)
            {

              if(gameObject.GetComponent<InitStartup>().changeInRepublicanPVI.Contains(clickIdentifier))
                {
                  specialText = "This precinct flipped from Democratic to Republican over these years.";
                }

              if(gameObject.GetComponent<InitStartup>().changeInDemocraticPVI.Contains(clickIdentifier))
                {
                  specialText = "This precinct flipped from Republican to Democratic over these years.";
                }
            }

          else if(gameObject.GetComponent<UIManager>().specialFuncSelector == 4)
            {

              string turnoutThisYear = "";

              if(gameObject.GetComponent<UIManager>().doingTwoYears == false)
                {
                  turnoutThisYear = "" + (float)gameObject.GetComponent<InitStartup>().turnoutPercentage[clickIdentifier];

                  specialText = "Turnout in this precinct was " + turnoutThisYear + "% during this year.";
                }

              if(gameObject.GetComponent<UIManager>().doingTwoYears == true)
                {
                  turnoutThisYear = "" + (float)gameObject.GetComponent<InitStartup>().turnoutPercentage[clickIdentifier];

                  specialText = "Turnout in this precinct changed by " + turnoutThisYear + "% during these years.";
                }
            }
        }



















        public void demoDeterminer()
          {
            if(gameObject.GetComponent<UIManager>().demoDataSelector == 1)
              {
                demoIdentifier = "White";
              }

            if(gameObject.GetComponent<UIManager>().demoDataSelector == 2)
              {
                demoIdentifier = "African American";
              }

            if(gameObject.GetComponent<UIManager>().demoDataSelector == 3)
              {
                demoIdentifier = "Asian American";
              }

            if(gameObject.GetComponent<UIManager>().demoDataSelector == 4)
              {
                demoIdentifier = "Hispanic American";
              }

            if(gameObject.GetComponent<UIManager>().genderDataSelector == 1)
              {
                demoIdentifier = "Male";
              }

            if(gameObject.GetComponent<UIManager>().genderDataSelector == 2)
              {
                demoIdentifier = "Female";
              }
          }
    }
