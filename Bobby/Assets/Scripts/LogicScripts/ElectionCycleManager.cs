using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectionCycleManager : MonoBehaviour
{


 /// Who won/lost

 private float electionMeasureRep;
 private float electionMeasureDem;

 /// Election Turnout

 private float electionOutcomeRepublican;
 private float electionOutcomeDemocrats;
 private float electionTotalVotes;
 private float electionTotalRegistered;
 private float electionVoterTurnout;


 /// Percentages

 private float electionDemocratWinningMargin;
 private float electionRepublicanWiningMargin;
 private float electionDemocratPercentage;
 private float electionRepublicanPercentage;


//// Color Stuff

private float DemColor = 0f;
private float RepColor = 0f;
private float colorConstant;

/// Stuff for the split precincts

 public GameObject precinctColorChangeGameObject;
 public GameObject[] precinctColorChangeGameObjectsForSplitPrecincts;
 private string precinctColorChangeGameObjectFinder;
 private bool currentPrecicntIsSplit;

/// Hashtables for the year picker stuff

 public Hashtable dictionaryYearToLookAtRepublicanYearOne = new Hashtable();
 public Hashtable dictionaryYearToLookAtDemocraticYearOne = new Hashtable();
 public Hashtable dictionaryYearToLookAtRegisteredVotersYearOne = new Hashtable();
 public Hashtable dictionaryYearToLookAtCastedBallotsYearOne = new Hashtable();

 public Hashtable dictionaryYearToLookAtRepublicanYearTwo = new Hashtable();
 public Hashtable dictionaryYearToLookAtDemocraticYearTwo = new Hashtable();
 public Hashtable dictionaryYearToLookAtRegisteredVotersYearTwo = new Hashtable();
 public Hashtable dictionaryYearToLookAtCastedBallotsYearTwo = new Hashtable();

 public Hashtable dictionaryYearToLookAtDemocraticPVI = new Hashtable();
 public Hashtable dictionaryYearToLookAtRepublicanPVI= new Hashtable();
 public Hashtable dictionaryYearToLookAtDemocraticPVITwo = new Hashtable();
 public Hashtable dictionaryYearToLookAtRepublicanPVITwo = new Hashtable();

 public Hashtable dictionaryYearToLookAtCastedBallotsOne = new Hashtable();
 public Hashtable dictionaryYearToLookAtCastedBallotsTwo = new Hashtable();
 public Hashtable dictionaryYearToLookAtRegisteredVotersOne = new Hashtable();
 public Hashtable dictionaryYearToLookAtRegisteredVotersTwo = new Hashtable();


 public float averageStatewideResultsDemocrat;
 public float averageStatewideResultsRepublican;



 public void implimentCastedBallotsVisualization()
  {
    foreach(DictionaryEntry castedBalInPrecinct in dictionaryYearToLookAtCastedBallotsOne)
      {
        precinctColorChangeGameObjectFinder = "" + castedBalInPrecinct.Key;

        if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
         {
           precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
           currentPrecicntIsSplit = true;
         }

        else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
         {
         //  Debug.Log(precinctColorChangeGameObjectFinder);
           precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
         }



        float castedBalInPrecinctYearOneFloat = (float)castedBalInPrecinct.Value;

        if(dictionaryYearToLookAtCastedBallotsTwo.ContainsKey(castedBalInPrecinct.Key))
          {
            float castedBalInPrecinctYearTwoFloat = (float)dictionaryYearToLookAtCastedBallotsTwo[castedBalInPrecinct.Key];

            float changeInCastedBallotsBetweenYears = castedBalInPrecinctYearTwoFloat - castedBalInPrecinctYearOneFloat;

            gameObject.GetComponent<InitStartup>().changeInCastedBallots.Add("" + castedBalInPrecinct.Key, changeInCastedBallotsBetweenYears);



            float changeInCastedBalPercentage = castedBalInPrecinctYearTwoFloat / castedBalInPrecinctYearOneFloat;

            Debug.Log("Change in Percentage Before Sub " + changeInCastedBalPercentage);

            changeInCastedBalPercentage -= 1;

            Debug.Log("Change in Percentage After Sub " + changeInCastedBalPercentage);


            if(changeInCastedBalPercentage > 0)
              {

                if(precinctColorChangeGameObject != null)
                  {

                    float tempColorVar = changeInCastedBalPercentage;

                    tempColorVar = 1 - tempColorVar;

                    if(currentPrecicntIsSplit == false)
                      {
                        precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (tempColorVar, 1, tempColorVar, 1);
                      }

                    if(currentPrecicntIsSplit == true)
                      {
                        foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                          {
                            splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (tempColorVar, 1, tempColorVar, 1);
                          }

                          precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                          currentPrecicntIsSplit = false;
                      }
                  }
              }
            else if (changeInCastedBalPercentage < 0)
              {

                float tempColorVar = changeInCastedBalPercentage;

                tempColorVar *= -1;

                tempColorVar = 1 - tempColorVar;


                if(precinctColorChangeGameObject != null)
                  {
                    if(currentPrecicntIsSplit == false)
                      {
                        precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (1, tempColorVar, tempColorVar, 1);
                      }

                    if(currentPrecicntIsSplit == true)
                      {
                        foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                          {
                            splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (1, tempColorVar, tempColorVar, 1);
                          }

                          precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                          currentPrecicntIsSplit = false;
                      }
                  }
              }

          changeInCastedBalPercentage *= 100;

                      gameObject.GetComponent<InitStartup>().changeinCastedBalPercentageTable.Add("" + castedBalInPrecinct.Key, changeInCastedBalPercentage);

          }
      }
  }

 public void implimentTurnoutVisualizationTwoYears()
   {
     /// Year one is the starting year. Ex. If trying to look between 2018-2021, Year One is 2018
     foreach(DictionaryEntry electionResultsTurnoutCycleTwo in dictionaryYearToLookAtCastedBallotsOne)
       {
         float electionCastedBallotsYearOne = (float)electionResultsTurnoutCycleTwo.Value;

         if(dictionaryYearToLookAtCastedBallotsTwo.ContainsKey(electionResultsTurnoutCycleTwo.Key))
           {
             Debug.Log(electionResultsTurnoutCycleTwo.Key);

             float electionCastedBallotsYearTwo = (float)dictionaryYearToLookAtCastedBallotsOne[electionResultsTurnoutCycleTwo.Key];

           //  Debug.Log("For the second year we've got: " + electionMeasureRepYearTwo);

             float electionRegVotersYearOne = (float)dictionaryYearToLookAtRegisteredVotersOne[electionResultsTurnoutCycleTwo.Key];

             float electionRegVotersYearTwo = (float)dictionaryYearToLookAtRegisteredVotersTwo[electionResultsTurnoutCycleTwo.Key];


             float electionTurnoutYrOne = electionCastedBallotsYearOne / electionRegVotersYearOne;

             float electionTurnoutYtTwo = electionCastedBallotsYearTwo / electionRegVotersYearTwo;

          //   Debug.Log("Turnout One " + electionTurnoutYrOne + ". Turnout 2: " + electionTurnoutYtTwo);

             float overChangeInTurnout = electionTurnoutYtTwo - electionTurnoutYrOne;

             overChangeInTurnout *= 100;

             gameObject.GetComponent<InitStartup>().turnoutPercentage.Add("" + electionResultsTurnoutCycleTwo.Key, overChangeInTurnout);

             overChangeInTurnout /= 100;

             Debug.Log(overChangeInTurnout);

             precinctColorChangeGameObjectFinder = "" + electionResultsTurnoutCycleTwo.Key;

             if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
             {
               precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
               currentPrecicntIsSplit = true;
             }

             else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
             {
               //  Debug.Log(precinctColorChangeGameObjectFinder);
               precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
             }


             if(overChangeInTurnout > 0) /// Turnout increased
               {
                 //Debug.Log(precinctColorChangeGameObject);

                 float TurnoutColorPicker = 1 - overChangeInTurnout;

                   if(precinctColorChangeGameObject != null)
                     {
                       if(currentPrecicntIsSplit == false)
                         {
                           precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (TurnoutColorPicker, 1, TurnoutColorPicker, 1);
                         }

                       if(currentPrecicntIsSplit == true)
                         {
                           foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                             {
                               splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (TurnoutColorPicker, 1, TurnoutColorPicker, 1);
                             }

                             precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                             currentPrecicntIsSplit = false;
                         }
                     }
               }


             if(overChangeInTurnout < 0) /// Turnout Decreased
               {
                 //Debug.Log(precinctColorChangeGameObject);

                 overChangeInTurnout *= -1;

                 float TurnoutColorPicker = 1 - overChangeInTurnout;

                   if(precinctColorChangeGameObject != null)
                     {
                       if(currentPrecicntIsSplit == false)
                         {
                           precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (1, TurnoutColorPicker, TurnoutColorPicker, 1);
                         }

                       if(currentPrecicntIsSplit == true)
                         {
                           foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                             {
                               splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (1, TurnoutColorPicker, TurnoutColorPicker, 1);
                             }

                             precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                             currentPrecicntIsSplit = false;
                         }
                     }
               }
           }
       }
   }


 public void implimentTurnoutVisualization()
  {
    foreach(DictionaryEntry turnoutInPrecinct in dictionaryYearToLookAtCastedBallotsOne)
      {
        precinctColorChangeGameObjectFinder = "" + turnoutInPrecinct.Key;

        if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
         {
           precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
           currentPrecicntIsSplit = true;
         }

        else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
         {
         //  Debug.Log(precinctColorChangeGameObjectFinder);
           precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
         }

         electionTotalRegistered = (float)dictionaryYearToLookAtRegisteredVotersOne[turnoutInPrecinct.Key];

      //   Debug.Log("" + electionTotalRegistered);

         electionTotalVotes = (float)dictionaryYearToLookAtCastedBallotsOne[turnoutInPrecinct.Key];

         electionVoterTurnout = electionTotalVotes / electionTotalRegistered;

         electionVoterTurnout *= 100;

         gameObject.GetComponent<InitStartup>().turnoutPercentage.Add("" + turnoutInPrecinct.Key, electionVoterTurnout);

         electionVoterTurnout /= 100;

         float TurnoutColorPicker = 1;

         TurnoutColorPicker -= electionVoterTurnout;

         TurnoutColorPicker *= 1.5f;

         if(precinctColorChangeGameObject != null)
          {
            if(currentPrecicntIsSplit == false) //// And not a split into multile GO precinct
              {
                precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (TurnoutColorPicker, 1, TurnoutColorPicker, 1); /// Change the color of the sprite rend
              }

            if(currentPrecicntIsSplit == true) /// And if it IS split into multiple GO
              {
                foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts) /// For each split precinct in the split precinct array, which should be filled with all the game objects that make up this preinct in the code above
                  {
                    splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (TurnoutColorPicker, 1, TurnoutColorPicker, 1); /// Change the color of all their sprite rends
                  }

                  precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0]; /// Set the array back to 0 just to make sure no fuckery happens
                  currentPrecicntIsSplit = false; //// The next precinct might not be split so set that to false
              }
          }
      }
  }

    public void implimentPVICalcsOverTwoYears()
      {

        foreach(DictionaryEntry pviOfPrecinct in dictionaryYearToLookAtRepublicanPVI)
          {


            precinctColorChangeGameObjectFinder = "" + pviOfPrecinct.Key; // Changes to string

            if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
             {
               precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
               currentPrecicntIsSplit = true;
             }

            else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
             {
             //  Debug.Log(precinctColorChangeGameObjectFinder);
               precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
             }

             Debug.Log(precinctColorChangeGameObjectFinder);


            if(dictionaryYearToLookAtRepublicanPVITwo.ContainsKey(pviOfPrecinct.Key)) /// If stayed Republican
              {
                float pviYearOne = (float)pviOfPrecinct.Value;

                float pviYearTwo = (float)dictionaryYearToLookAtRepublicanPVITwo[pviOfPrecinct.Key];

                float finalPVI = pviYearTwo - pviYearOne;

                if(finalPVI < 0) /// Shifted left, still Rep
                  {
                    finalPVI *= -1;
                    gameObject.GetComponent<InitStartup>().changeInRepublicanPVINeg.Add("" + pviOfPrecinct.Key, finalPVI);

                    if(precinctColorChangeGameObject != null)
                      {
                        float repColorFixer = 1 - (finalPVI / 15);

                        if(currentPrecicntIsSplit == false)
                          {
                            precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (repColorFixer, repColorFixer, 1, 1);
                          }

                          if(currentPrecicntIsSplit == true)
                          {
                            foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                              {
                                splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (repColorFixer, repColorFixer, 1, 1);
                              }

                              precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                              currentPrecicntIsSplit = false;
                        }
                    }
                  }

                else if(finalPVI > 0) /// Shifted Right, still Rep
                  {
                    gameObject.GetComponent<InitStartup>().changeInRepublicanPVI.Add("" + pviOfPrecinct.Key, finalPVI);

                    if(precinctColorChangeGameObject != null)
                      {
                        float repColorFixer = 1 - (finalPVI / 15);

                        if(currentPrecicntIsSplit == false)
                          {
                            precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (1, repColorFixer, repColorFixer, 1);
                          }

                          if(currentPrecicntIsSplit == true)
                          {
                            foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                              {
                                splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (1, repColorFixer, repColorFixer, 1);
                              }

                              precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                              currentPrecicntIsSplit = false;
                        }
                    }
                  }
              }

            if(dictionaryYearToLookAtDemocraticPVITwo.ContainsKey(pviOfPrecinct.Key)) /// Flipped Dem
              {
                float pviYearOne = (float)pviOfPrecinct.Value;

                float pviYearTwo = (float)dictionaryYearToLookAtDemocraticPVITwo[pviOfPrecinct.Key];

                float finalPVI = pviYearOne + pviYearTwo;

                gameObject.GetComponent<InitStartup>().changeInPVIDemFlip.Add("" + pviOfPrecinct.Key, finalPVI);

                if(precinctColorChangeGameObject != null)
                  {
                    float repColorFixer = 1 - (finalPVI / 15);

                    if(currentPrecicntIsSplit == false)
                      {
                        precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (repColorFixer, repColorFixer, 1, 1);
                      }

                      if(currentPrecicntIsSplit == true)
                      {
                        foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                          {
                            splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (repColorFixer, repColorFixer, 1, 1);
                          }

                          precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                          currentPrecicntIsSplit = false;
                    }
                }
              }
          }


        foreach(DictionaryEntry pviOfPrecinct in dictionaryYearToLookAtDemocraticPVI)
          {

            precinctColorChangeGameObjectFinder = "" + pviOfPrecinct.Key; // Changes to string

            if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
             {
               precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
               currentPrecicntIsSplit = true;
             }

            else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
             {
             //  Debug.Log(precinctColorChangeGameObjectFinder);
               precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
             }


            if(dictionaryYearToLookAtDemocraticPVITwo.ContainsKey(pviOfPrecinct.Key)) /// If stayed Dem
              {
                float pviYearOne = (float)pviOfPrecinct.Value;

                float pviYearTwo = (float)dictionaryYearToLookAtDemocraticPVITwo[pviOfPrecinct.Key];

                float finalPVI = pviYearTwo - pviYearOne;

                if(finalPVI < 0) /// Shifted right, still Dem
                  {
                    finalPVI *= -1;
                    gameObject.GetComponent<InitStartup>().changeInDemocraticPVINeg.Add("" + pviOfPrecinct.Key, finalPVI);

                    if(precinctColorChangeGameObject != null)
                      {
                        float repColorFixer = 1 - (finalPVI / 15);

                        if(currentPrecicntIsSplit == false)
                          {
                            precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (1, repColorFixer, repColorFixer, 1);
                          }

                          if(currentPrecicntIsSplit == true)
                          {
                            foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                              {
                                splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (1, repColorFixer, repColorFixer, 1);
                              }

                              precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                              currentPrecicntIsSplit = false;
                        }
                    }
                  }

                else if(finalPVI > 0) /// Shifted Left, still Dem
                  {
                    gameObject.GetComponent<InitStartup>().changeInDemocraticPVI.Add("" + pviOfPrecinct.Key, finalPVI);

                    if(precinctColorChangeGameObject != null)
                      {
                        float repColorFixer = 1 - (finalPVI / 15);

                        if(currentPrecicntIsSplit == false)
                          {
                            precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (repColorFixer, repColorFixer, 1, 1);
                          }

                          if(currentPrecicntIsSplit == true)
                          {
                            foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                              {
                                splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (repColorFixer, repColorFixer, 1, 1);
                              }

                              precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                              currentPrecicntIsSplit = false;
                        }
                    }
                  }
              }

            if(dictionaryYearToLookAtRepublicanPVITwo.ContainsKey(pviOfPrecinct.Key)) /// Flipped Rep
              {
                float pviYearOne = (float)pviOfPrecinct.Value;

                float pviYearTwo = (float)dictionaryYearToLookAtRepublicanYearTwo[pviOfPrecinct.Key];

                float finalPVI = pviYearOne + pviYearTwo;

                gameObject.GetComponent<InitStartup>().changeInPVIRepFlip.Add("" + pviOfPrecinct.Key, finalPVI);


                if(precinctColorChangeGameObject != null)
                  {
                    float repColorFixer = 1 - (finalPVI / 15);

                    if(currentPrecicntIsSplit == false)
                      {
                        precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (1, repColorFixer, repColorFixer, 1);
                      }

                      if(currentPrecicntIsSplit == true)
                      {
                        foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                          {
                            splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (1, repColorFixer, repColorFixer, 1);
                          }

                          precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                          currentPrecicntIsSplit = false;
                    }
                }
              }
          }
      }

     public void implimentPVICalculations()
      {

        foreach(DictionaryEntry pviOfPrecinct in dictionaryYearToLookAtRepublicanPVI)
          {
            float repPVI = (float)pviOfPrecinct.Value;


            precinctColorChangeGameObjectFinder = "" + pviOfPrecinct.Key; // Changes to string

            if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
             {
               precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
               currentPrecicntIsSplit = true;
             }

            else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
             {
             //  Debug.Log(precinctColorChangeGameObjectFinder);
               precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
             }

             gameObject.GetComponent<InitStartup>().republicanPVI.Add("" + pviOfPrecinct.Key, repPVI);

              if(precinctColorChangeGameObject != null)
                {
                  float repColorFixer = 1 - (repPVI / 10);

                  if(currentPrecicntIsSplit == false)
                    {
                      precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (1, repColorFixer, repColorFixer, 1);
                    }

                    if(currentPrecicntIsSplit == true)
                    {
                      foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                        {
                          splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (1, repColorFixer, repColorFixer, 1);
                        }

                        precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                        currentPrecicntIsSplit = false;
                  }
              }
            }


        foreach(DictionaryEntry pviOfPrecinct in dictionaryYearToLookAtDemocraticPVI)
          {
            float demPVI = (float)pviOfPrecinct.Value;


            precinctColorChangeGameObjectFinder = "" + pviOfPrecinct.Key; // Changes to string

            if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
             {
               precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
               currentPrecicntIsSplit = true;
             }

            else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
             {
             //  Debug.Log(precinctColorChangeGameObjectFinder);
               precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
             }

             gameObject.GetComponent<InitStartup>().democraticPVI.Add("" + pviOfPrecinct.Key, demPVI);

              if(precinctColorChangeGameObject != null)
                {
                  float demColorFixer = 1 - (demPVI / 10);

                  if(currentPrecicntIsSplit == false)
                    {
                      precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (demColorFixer, demColorFixer, 1, 1);
                    }

                    if(currentPrecicntIsSplit == true)
                    {
                      foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                        {
                          splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (demColorFixer, demColorFixer, 1, 1);
                        }

                        precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                        currentPrecicntIsSplit = false;
                    }
                  }
                }
              }
     public void implimentElectionLayoutTwocycle ()
      {
        foreach(DictionaryEntry electionResults in dictionaryYearToLookAtRepublicanYearOne)
          {
            float electionMeasureRepYearOne = (float)electionResults.Value;

            float electionMeasureDemYearOne = (float)dictionaryYearToLookAtDemocraticYearOne[electionResults.Key];

            float electionTotalRegisteredYearOne = (float)dictionaryYearToLookAtRegisteredVotersYearOne[electionResults.Key];

            float electionTotalVotesYearOne = (float)dictionaryYearToLookAtCastedBallotsYearOne[electionResults.Key];

            float electionVoterTurnoutYearOne = electionTotalVotesYearOne / electionTotalRegisteredYearOne;

            electionVoterTurnoutYearOne *= 100;

            if(dictionaryYearToLookAtRepublicanYearTwo.Contains(electionResults.Key))
              {
                float electionMeasureRepYearTwo = (float)dictionaryYearToLookAtRepublicanYearTwo[electionResults.Key];

                float electionMeasureDemYearTwo = (float)dictionaryYearToLookAtDemocraticYearTwo[electionResults.Key];

                float electionTotalRegisteredYearTwo = (float)dictionaryYearToLookAtRegisteredVotersYearTwo[electionResults.Key];

                float electionTotalVotesYearTwo = (float)dictionaryYearToLookAtCastedBallotsYearTwo[electionResults.Key];

                float electionVoterTurnoutYearTwo = electionTotalVotesYearTwo / electionTotalRegisteredYearTwo;

                electionVoterTurnoutYearTwo *= 100;

                float electionPercentageRepYearOne = electionMeasureRepYearOne / electionTotalVotesYearOne;

                float electionPercentageRepYearTwo = electionMeasureRepYearTwo / electionTotalVotesYearTwo;

                float electionPercentageDemYearOne = electionMeasureDemYearOne / electionTotalVotesYearOne;

                float electionPercentageDemYearTwo = electionMeasureDemYearTwo / electionTotalVotesYearTwo;

                electionPercentageRepYearOne *= 100;

                electionPercentageRepYearTwo *= 100;

                electionPercentageDemYearOne *= 100;

                electionPercentageDemYearTwo *= 100;

                electionTotalVotesYearTwo -= electionTotalVotesYearOne;

                electionVoterTurnoutYearTwo -= electionVoterTurnoutYearOne;

                Debug.Log("Two Year Turnout dif: " + electionVoterTurnoutYearTwo);

                gameObject.GetComponent<InitStartup>().changeInCastedBallots.Add("" + electionResults.Key, electionTotalVotesYearTwo);
                gameObject.GetComponent<InitStartup>().changeInTurnout.Add("" + electionResults.Key, electionVoterTurnoutYearTwo);





                Debug.Log("Currently on " + electionResults.Key + "for two years. If it stops after this then this is the one that's broken.");

                precinctColorChangeGameObjectFinder = "" + electionResults.Key; // Changes to string

                if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
                 {
                   precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
                   currentPrecicntIsSplit = true;
                 }

                else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
                 {
                 //  Debug.Log(precinctColorChangeGameObjectFinder);
                   precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
                 }


                 electionPercentageRepYearTwo = electionPercentageRepYearTwo - electionPercentageRepYearOne;

                 electionPercentageDemYearTwo = electionPercentageDemYearTwo- electionPercentageDemYearOne;


                 if(precinctColorChangeGameObject != null)
                  {

                    float colorRed = 0;
                    float colorBlue = 0;
                    float colorGreen = 0;

                    if(electionPercentageRepYearTwo > electionMeasureDemYearTwo || electionPercentageDemYearTwo < 0) /// Became More Rep
                      {

                        gameObject.GetComponent<InitStartup>().changeInMarginRepublican.Add("" + electionResults.Key, electionPercentageRepYearTwo);

                        electionPercentageRepYearTwo /= 100;

                        colorRed = 1;
                        colorBlue = 1 - (electionPercentageRepYearTwo * 2.5f);
                        colorGreen = 1 - (electionPercentageRepYearTwo * 2.5f);

                      }

                    else if(electionMeasureDemYearTwo > electionPercentageRepYearOne) /// Became More Dem
                      {

                        gameObject.GetComponent<InitStartup>().changeInMarginDemocratic.Add("" + electionResults.Key, electionPercentageDemYearTwo);

                        electionPercentageDemYearTwo /= 100;

                        colorRed = 1 - (electionPercentageDemYearTwo * 2.5f);
                        colorBlue = 1;
                        colorGreen = 1 - (electionPercentageDemYearTwo * 2.5f);
                      }




                      if(currentPrecicntIsSplit == false) //// And not a split into multile GO precinct
                        {
                          precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (colorRed, colorGreen, colorBlue, 1); /// Change the color of the sprite rend
                        }

                        if(currentPrecicntIsSplit == true) /// And if it IS split into multiple GO
                          {
                            foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts) /// For each split precinct in the split precinct array, which should be filled with all the game objects that make up this preinct in the code above
                              {
                                splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (colorRed, colorGreen, colorBlue, 1); /// Change the color of all their sprite rends
                              }

                              precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0]; /// Set the array back to 0 just to make sure no fuckery happens
                              currentPrecicntIsSplit = false; //// The next precinct might not be split so set that to false
                          }
                  }
              }
          }
      }
     public void implimentElectionLayoutOneCycle ()
       {

              foreach(DictionaryEntry electionResults in dictionaryYearToLookAtRepublicanYearOne)
                {

                  electionMeasureRep = (float)electionResults.Value;

            //      Debug.Log("Working bc we're currently on " + electionResults.Key + ". If stops after this, this is the broken precinct");

                //  Debug.Log("" + electionMeasureRep);


                      electionMeasureDem = (float)dictionaryYearToLookAtDemocraticYearOne[electionResults.Key];

                  //    Debug.Log("" + electionMeasureDem);


                         electionTotalRegistered = (float)dictionaryYearToLookAtRegisteredVotersYearOne[electionResults.Key];

                      //   Debug.Log("" + electionTotalRegistered);

                         electionTotalVotes = (float)dictionaryYearToLookAtCastedBallotsYearOne[electionResults.Key];

                         electionVoterTurnout = electionTotalVotes / electionTotalRegistered;

                         electionVoterTurnout = electionVoterTurnout * 100;

                         gameObject.GetComponent<InitStartup>().turnoutPercentage.Add("" + electionResults.Key, electionVoterTurnout);

                         gameObject.GetComponent<InitStartup>().castedBallots.Add("" + electionResults.Key, electionTotalVotes);



                      // Debug.Log("Currently on " + electionResults.Key + ", if it stops after this then this is the one that's broken.");


                         precinctColorChangeGameObjectFinder = "" + electionResults.Key; // Changes to string

                         if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
                          {
                            precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
                            currentPrecicntIsSplit = true;
                          }

                         else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
                          {
                          //  Debug.Log(precinctColorChangeGameObjectFinder);
                            precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
                          }

                         if(precinctColorChangeGameObject == null)
                          {
                            Debug.Log("Precinct " + electionResults.Key + " doesn't have a game object");
                          }

                         if(precinctColorChangeGameObject != null)
                            {
                              if(electionMeasureRep > electionMeasureDem) //// If Reps won
                                {

                                  percentageCalculator();

                                  RepColor = 1 - (electionRepublicanPercentage);

                                  RepColor *= 1.5f;

                                  standerizePercentages(); /// Multiplies the percentages by 100

                                  electionRepublicanWiningMargin = electionMeasureRep - electionMeasureDem;

                                  gameObject.GetComponent<InitStartup>().marginOfWinRepublicanWin.Add("" + electionResults.Key, electionRepublicanWiningMargin); /// Adds it to the Display hashtable for Republican wins

                                  gameObject.GetComponent<InitStartup>().percentageDemocratic.Add("" + electionResults.Key, electionDemocratPercentage); /// Adds it to the Display hashtable for Democrat percentage

                                  gameObject.GetComponent<InitStartup>().percentageRepublican.Add("" + electionResults.Key, electionRepublicanPercentage); /// Adds it to the Display hashtable for Republican percentage




                                 if(currentPrecicntIsSplit == false) //// And not a split into multile GO precinct
                                   {
                                     precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (1, RepColor, RepColor, 1); /// Change the color of the sprite rend
                                   }




                                 if(currentPrecicntIsSplit == true) /// And if it IS split into multiple GO
                                   {
                                     foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts) /// For each split precinct in the split precinct array, which should be filled with all the game objects that make up this preinct in the code above
                                       {
                                         splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (1, RepColor, RepColor, 1); /// Change the color of all their sprite rends
                                       }

                                       precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0]; /// Set the array back to 0 just to make sure no fuckery happens
                                       currentPrecicntIsSplit = false; //// The next precinct might not be split so set that to false
                                   }
                                }



                               else if (electionMeasureRep < electionMeasureDem) ///// If Dems won
                                 {

                                   percentageCalculator(); /// Calc the percentages

                                   DemColor = 1 - (electionDemocratPercentage);

                                   DemColor *= 1.5f;

                                   standerizePercentages(); /// Multiples the percentages by 100

                                   electionDemocratWinningMargin = electionMeasureDem - electionMeasureRep; //// Calculate the Margin of win

                                   gameObject.GetComponent<InitStartup>().marginOfWinDemocratWin.Add("" + electionResults.Key, electionDemocratWinningMargin); /// Adds it to the Display hashtable for Republican wins

                                   gameObject.GetComponent<InitStartup>().percentageDemocratic.Add("" + electionResults.Key, electionDemocratPercentage); /// Adds it to the Display hashtable for Democrat percentage

                                   gameObject.GetComponent<InitStartup>().percentageRepublican.Add("" + electionResults.Key, electionRepublicanPercentage); /// Adds it to the Display hashtable for Republican percentage



                                   if(currentPrecicntIsSplit == false)
                                     {
                                       precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (DemColor, DemColor, 1, 1);
                                     }



                                   if(currentPrecicntIsSplit == true)
                                     {
                                       foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                                         {
                                           splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (DemColor, DemColor, 1, 1);
                                         }

                                         precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                                         currentPrecicntIsSplit = false;
                                     }


                                 }

                               else if (electionMeasureRep == electionMeasureDem)
                                 {

                                   if(currentPrecicntIsSplit == false)
                                     {
                                       precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (0, 1, 0, 1);
                                     }

                                   if(currentPrecicntIsSplit == true)
                                     {
                                       foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                                         {
                                           splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (0, 1, 0, 1);
                                         }

                                         precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                                         currentPrecicntIsSplit = false;
                                     }


                                   gameObject.GetComponent<InitStartup>().tiedPrecinct.Add("" + electionResults.Key, 0);
                               //    gameObject.GetComponent<UIManager>().dataTextBox.text = gameObject.GetComponent<UIManager>().dataTextBox.text + "Democrats and Republicans tied in precinct " + electionResults.Key;
                                 }
                            }
              }
            }

      public void percentageCalculator()
        {
          electionDemocratPercentage = electionMeasureDem / electionTotalVotes;

          electionRepublicanPercentage = electionMeasureRep / electionTotalVotes;
        }

        public void standerizePercentages()
          {
            electionRepublicanPercentage = electionRepublicanPercentage * 100;

            electionDemocratPercentage = electionDemocratPercentage * 100;
          }






      public void implimentFlipsLayoutTwoCycles()
        {
          /// Year one is the starting year. Ex. If trying to look between 2018-2021, Year One is 2018
          foreach(DictionaryEntry electionResultsCycleTwo in dictionaryYearToLookAtRepublicanYearOne)
            {
              float electionMeasureRepYearOne = (float)electionResultsCycleTwo.Value;

            //  Debug.Log(electionMeasureRepYearOne);

              float electionMeasureDemYearOne = (float)dictionaryYearToLookAtDemocraticYearOne[electionResultsCycleTwo.Key];

              if(dictionaryYearToLookAtRepublicanYearTwo.ContainsKey(electionResultsCycleTwo.Key))
                {
                  float electionMeasureRepYearTwo = (float)dictionaryYearToLookAtRepublicanYearTwo[electionResultsCycleTwo.Key];

                //  Debug.Log("For the second year we've got: " + electionMeasureRepYearTwo);

                  float electionMeasureDemYearTWo = (float)dictionaryYearToLookAtDemocraticYearTwo[electionResultsCycleTwo.Key];

                  precinctColorChangeGameObjectFinder = "" + electionResultsCycleTwo.Key;


                  if(electionMeasureRepYearOne < electionMeasureDemYearOne && electionMeasureDemYearTWo < electionMeasureRepYearTwo) /// Looking at Rep flips
                    {
                      //Debug.Log(precinctColorChangeGameObject);

                      gameObject.GetComponent<InitStartup>().changeInRepublicanPVI.Add("" + electionResultsCycleTwo.Key, electionMeasureRepYearOne);

                        if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
                        {
                          precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
                          currentPrecicntIsSplit = true;
                        }

                        else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
                        {
                          //  Debug.Log(precinctColorChangeGameObjectFinder);
                          precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
                        }

                        if(precinctColorChangeGameObject != null)
                          {
                            if(currentPrecicntIsSplit == false)
                              {
                                precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (1, 0, 0, 1);
                              }

                            if(currentPrecicntIsSplit == true)
                              {
                                foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                                  {
                                    splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (1, 0, 0, 1);
                                  }

                                  precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                                  currentPrecicntIsSplit = false;
                              }
                          }
                    }


                  if(electionMeasureDemYearOne < electionMeasureRepYearOne && electionMeasureRepYearTwo < electionMeasureDemYearTWo) /// Looking at Dem flips
                    {
                      //Debug.Log(precinctColorChangeGameObject);

                      gameObject.GetComponent<InitStartup>().changeInDemocraticPVI.Add("" + electionResultsCycleTwo.Key, electionMeasureRepYearOne);

                        if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
                        {
                          precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
                          currentPrecicntIsSplit = true;
                        }

                        else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
                        {
                          //  Debug.Log(precinctColorChangeGameObjectFinder);
                          precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);
                        }

                        if(precinctColorChangeGameObject != null)
                          {
                            if(currentPrecicntIsSplit == false)
                              {
                                precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color (0, 0, 1, 1);
                              }

                            if(currentPrecicntIsSplit == true)
                              {
                                foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                                  {
                                    splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (0, 0, 1, 1);
                                  }

                                  precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                                  currentPrecicntIsSplit = false;
                              }
                          }
                    }
                }
            }
        }
}
