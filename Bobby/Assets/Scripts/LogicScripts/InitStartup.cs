using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitStartup : MonoBehaviour
{



    ///////////////////////////////////////////
    //// NEWEST HASHTABLES
    ///////////////////////////////////////////

    ////////////// REPUBLICAN VOTES

    public Hashtable republicanVotes2021 = new Hashtable();
    public Hashtable republicanVotes2020 = new Hashtable();
    public Hashtable republicanVotes2018 = new Hashtable();
    public Hashtable republicanVotes2016 = new Hashtable();
    public Hashtable republicanVotes2012 = new Hashtable();

    ////////////// DEMOCRAT VOTES

    public Hashtable democratVotes2021 = new Hashtable();
    public Hashtable democratVotes2020 = new Hashtable();
    public Hashtable democratVotes2018 = new Hashtable();
    public Hashtable democratVotes2016 = new Hashtable();
    public Hashtable democratVotes2012 = new Hashtable();

    ////////////// REGISTERED VOTESR IN PRECINCT

    public Hashtable registeredVotersInPrecinct2021 = new Hashtable();
    public Hashtable registeredVotersInPrecinct2020 = new Hashtable();
    public Hashtable registeredVotersInPrecinct2018 = new Hashtable();
    public Hashtable registeredVotersInPrecinct2016 = new Hashtable();
    public Hashtable registeredVotersInPrecinct2012 = new Hashtable();

    ////////////// CAST BALLOTS IN PRECINCT

    public Hashtable castedBallotsInPrecinct2021 = new Hashtable();
    public Hashtable castedBallotsInPrecinct2020 = new Hashtable();
    public Hashtable castedBallotsInPrecinct2018 = new Hashtable();
    public Hashtable castedBallotsInPrecinct2016 = new Hashtable();
    public Hashtable castedBallotsInPrecinct2012 = new Hashtable();

    ////////////// Partisan Change

    public Hashtable partisanChangeBetweenTwoCycles = new Hashtable();


    ////////////// Voter Registration and Casted Ballots in Precinct by Race

    public Hashtable whiteRegisteredVotersInPrecinct2021 = new Hashtable();
    public Hashtable whiteRegisteredVotersInPrecinct2020 = new Hashtable();
    public Hashtable whiteRegisteredVotersInPrecinct2018 = new Hashtable();
    public Hashtable whiteRegisteredVotersInPrecinct2016 = new Hashtable();
    public Hashtable whiteRegisteredVotersInPrecinct2012 = new Hashtable();


    public Hashtable blackRegisteredVotersInPrecinct2021 = new Hashtable();
    public Hashtable blackRegisteredVotersInPrecinct2020 = new Hashtable();
    public Hashtable blackRegisteredVotersInPrecinct2018 = new Hashtable();
    public Hashtable blackRegisteredVotersInPrecinct2016 = new Hashtable();
    public Hashtable blackRegisteredVotersInPrecinct2012 = new Hashtable();


    public Hashtable asianRegisteredVotersInPrecinct2021 = new Hashtable();
    public Hashtable asianRegisteredVotersInPrecinct2020 = new Hashtable();
    public Hashtable asianRegisteredVotersInPrecinct2018 = new Hashtable();
    public Hashtable asianRegisteredVotersInPrecinct2016 = new Hashtable();
    public Hashtable asianRegisteredVotersInPrecinct2012 = new Hashtable();


    public Hashtable hispanicRegisteredVotersInPrecinct2021 = new Hashtable();
    public Hashtable hispanicRegisteredVotersInPrecinct2020 = new Hashtable();
    public Hashtable hispanicRegisteredVotersInPrecinct2018 = new Hashtable();
    public Hashtable hispanicRegisteredVotersInPrecinct2016 = new Hashtable();
    public Hashtable hispanicRegisteredVotersInPrecinct2012 = new Hashtable();


    public Hashtable whiteCastedBallotsInPrecinct2021 = new Hashtable();
    public Hashtable whiteCastedBallotsInPrecinct2020 = new Hashtable();
    public Hashtable whiteCastedBallotsInPrecinct2018 = new Hashtable();
    public Hashtable whiteCastedBallotsInPrecinct2016 = new Hashtable();
    public Hashtable whiteCastedBallotsInPrecinct2012 = new Hashtable();


    public Hashtable blackCastedBallotsInPrecinct2021 = new Hashtable();
    public Hashtable blackCastedBallotsInPrecinct2020 = new Hashtable();
    public Hashtable blackCastedBallotsInPrecinct2018 = new Hashtable();
    public Hashtable blackCastedBallotsInPrecinct2016 = new Hashtable();
    public Hashtable blackCastedBallotsInPrecinct2012 = new Hashtable();


    public Hashtable asianCastedBallotsInPrecinct2021 = new Hashtable();
    public Hashtable asianCastedBallotsInPrecinct2020 = new Hashtable();
    public Hashtable asianCastedBallotsInPrecinct2018 = new Hashtable();
    public Hashtable asianCastedBallotsInPrecinct2016 = new Hashtable();
    public Hashtable asianCastedBallotsInPrecinct2012 = new Hashtable();


    public Hashtable hispanicCastedBallotsInPrecinct2021 = new Hashtable();
    public Hashtable hispanicCastedBallotsInPrecinct2020 = new Hashtable();
    public Hashtable hispanicCastedBallotsInPrecinct2018 = new Hashtable();
    public Hashtable hispanicCastedBallotsInPrecinct2016 = new Hashtable();
    public Hashtable hispanicCastedBallotsInPrecinct2012 = new Hashtable();

    ////////////// Voter Registration and Casted Ballots in Precinct by Race and Gender

    public Hashtable whiteManRegVotersInPrecinct2021 = new Hashtable();
    public Hashtable whiteManRegVotersInPrecinct2020 = new Hashtable();
    public Hashtable whiteManRegVotersInPrecinct2018 = new Hashtable();
    public Hashtable whiteManRegVotersInPrecinct2016 = new Hashtable();
    public Hashtable whiteManRegVotersInPrecinct2012 = new Hashtable();


    public Hashtable whiteManCastedBalInPrecinct2021 = new Hashtable();
    public Hashtable whiteManCastedBalInPrecinct2020 = new Hashtable();
    public Hashtable whiteManCastedBalInPrecinct2018 = new Hashtable();
    public Hashtable whiteManCastedBalInPrecinct2016 = new Hashtable();
    public Hashtable whiteManCastedBalInPrecinct2012 = new Hashtable();


    public Hashtable whiteWomanRegVotersInPrecinct2021 = new Hashtable();
    public Hashtable whiteWomanRegVotersInPrecinct2020 = new Hashtable();
    public Hashtable whiteWomanRegVotersInPrecinct2018 = new Hashtable();
    public Hashtable whiteWomanRegVotersInPrecinct2016 = new Hashtable();
    public Hashtable whiteWomanRegVotersInPrecinct2012 = new Hashtable();


    public Hashtable whiteWomanCastedBalInPrecinct2021 = new Hashtable();
    public Hashtable whiteWomanCastedBalInPrecinct2020 = new Hashtable();
    public Hashtable whiteWomanCastedBalInPrecinct2018 = new Hashtable();
    public Hashtable whiteWomanCastedBalInPrecinct2016 = new Hashtable();
    public Hashtable whiteWomanCastedBalInPrecinct2012 = new Hashtable();


    public Hashtable blackManRegVotersInPrecinct2021 = new Hashtable();
    public Hashtable blackManRegVotersInPrecinct2020 = new Hashtable();
    public Hashtable blackManRegVotersInPrecinct2018 = new Hashtable();
    public Hashtable blackManRegVotersInPrecinct2016 = new Hashtable();
    public Hashtable blackManRegVotersInPrecinct2012 = new Hashtable();

    public Hashtable blackManCastedBalInPrecinct2021 = new Hashtable();
    public Hashtable blackManCastedBalInPrecinct2020 = new Hashtable();
    public Hashtable blackManCastedBalInPrecinct2018 = new Hashtable();
    public Hashtable blackManCastedBalInPrecinct2016 = new Hashtable();
    public Hashtable blackManCastedBalInPrecinct2012 = new Hashtable();

    public Hashtable blackWomanRegVotersInPrecinct2021 = new Hashtable();
    public Hashtable blackWomanRegVotersInPrecinct2020 = new Hashtable();
    public Hashtable blackWomanRegVotersInPrecinct2018 = new Hashtable();
    public Hashtable blackWomanRegVotersInPrecinct2016 = new Hashtable();
    public Hashtable blackWomanRegVotersInPrecinct2012 = new Hashtable();

    public Hashtable blackWomanCastedBalInPrecinct2021 = new Hashtable();
    public Hashtable blackWomanCastedBalInPrecinct2020 = new Hashtable();
    public Hashtable blackWomanCastedBalInPrecinct2018 = new Hashtable();
    public Hashtable blackWomanCastedBalInPrecinct2016 = new Hashtable();
    public Hashtable blackWomanCastedBalInPrecinct2012 = new Hashtable();


    public Hashtable asianManRegVotersInPrecinct2021 = new Hashtable();
    public Hashtable asianManRegVotersInPrecinct2020 = new Hashtable();
    public Hashtable asianManRegVotersInPrecinct2018 = new Hashtable();
    public Hashtable asianManRegVotersInPrecinct2016 = new Hashtable();
    public Hashtable asianManRegVotersInPrecinct2012 = new Hashtable();

    public Hashtable asianManCastedBalInPrecinct2021 = new Hashtable();
    public Hashtable asianManCastedBalInPrecinct2020 = new Hashtable();
    public Hashtable asianManCastedBalInPrecinct2018 = new Hashtable();
    public Hashtable asianManCastedBalInPrecinct2016 = new Hashtable();
    public Hashtable asianManCastedBalInPrecinct2012 = new Hashtable();

    public Hashtable asianWomanRegVotersInPrecinct2021 = new Hashtable();
    public Hashtable asianWomanRegVotersInPrecinct2020 = new Hashtable();
    public Hashtable asianWomanRegVotersInPrecinct2018 = new Hashtable();
    public Hashtable asianWomanRegVotersInPrecinct2016 = new Hashtable();
    public Hashtable asianWomanRegVotersInPrecinct2012 = new Hashtable();

    public Hashtable asianWomanCastedBalInPrecinct2021 = new Hashtable();
    public Hashtable asianWomanCastedBalInPrecinct2020 = new Hashtable();
    public Hashtable asianWomanCastedBalInPrecinct2018 = new Hashtable();
    public Hashtable asianWomanCastedBalInPrecinct2016 = new Hashtable();
    public Hashtable asianWomanCastedBalInPrecinct2012 = new Hashtable();


    public Hashtable hispanicManRegVotersInPrecinct2021 = new Hashtable();
    public Hashtable hispanicManRegVotersInPrecinct2020 = new Hashtable();
    public Hashtable hispanicManRegVotersInPrecinct2018 = new Hashtable();
    public Hashtable hispanicManRegVotersInPrecinct2016 = new Hashtable();
    public Hashtable hispanicManRegVotersInPrecinct2012 = new Hashtable();

    public Hashtable hispanicManCastedBalInPrecinct2021 = new Hashtable();
    public Hashtable hispanicManCastedBalInPrecinct2020 = new Hashtable();
    public Hashtable hispanicManCastedBalInPrecinct2018 = new Hashtable();
    public Hashtable hispanicManCastedBalInPrecinct2016 = new Hashtable();
    public Hashtable hispanicManCastedBalInPrecinct2012 = new Hashtable();

    public Hashtable hispanicWomanRegVotersInPrecinct2021 = new Hashtable();
    public Hashtable hispanicWomanRegVotersInPrecinct2020 = new Hashtable();
    public Hashtable hispanicWomanRegVotersInPrecinct2018 = new Hashtable();
    public Hashtable hispanicWomanRegVotersInPrecinct2016 = new Hashtable();
    public Hashtable hispanicWomanRegVotersInPrecinct2012 = new Hashtable();

    public Hashtable hispanicWomanCastedBalInPrecinct2021 = new Hashtable();
    public Hashtable hispanicWomanCastedBalInPrecinct2020 = new Hashtable();
    public Hashtable hispanicWomanCastedBalInPrecinct2018 = new Hashtable();
    public Hashtable hispanicWomanCastedBalInPrecinct2016 = new Hashtable();
    public Hashtable hispanicWomanCastedBalInPrecinct2012 = new Hashtable();


    ////////////// Population by Race

    public Hashtable whitePopulationOfPrecinct2021 = new Hashtable();
    public Hashtable whitePopulationOfPrecinct2020 = new Hashtable();
    public Hashtable whitePopulationOfPrecinct2018 = new Hashtable();
    public Hashtable whitePopulationOfPrecinct2016 = new Hashtable();
    public Hashtable whitePopulationOfPrecinct2012 = new Hashtable();

    public Hashtable blackPopulationOfPrecinct2021 = new Hashtable();
    public Hashtable blackPopulationOfPrecinct2020 = new Hashtable();
    public Hashtable blackPopulationOfPrecinct2018 = new Hashtable();
    public Hashtable blackPopulationOfPrecinct2016 = new Hashtable();
    public Hashtable blackPopulationOfPrecinct2012 = new Hashtable();

    public Hashtable asianPopulationOfPrecinct2021 = new Hashtable();
    public Hashtable asianPopulationOfPrecinct2020 = new Hashtable();
    public Hashtable asianPopulationOfPrecinct2018 = new Hashtable();
    public Hashtable asianPopulationOfPrecinct2016 = new Hashtable();
    public Hashtable asianPopulationOfPrecinct2012 = new Hashtable();

    public Hashtable hispanicPopulationOfPrecinct2021 = new Hashtable();
    public Hashtable hispanicPopulationOfPrecinct2020 = new Hashtable();
    public Hashtable hispanicPopulationOfPrecinct2018 = new Hashtable();
    public Hashtable hispanicPopulationOfPrecinct2016 = new Hashtable();
    public Hashtable hispanicPopulationOfPrecinct2012 = new Hashtable();


    /////////// Majority Precincts by Race

    public Hashtable majorityWhitePrecincts = new Hashtable();
    public Hashtable majorityBlackPrecincts = new Hashtable();
    public Hashtable majorityAsianPrecincts = new Hashtable();
    public Hashtable majorityHispanicPrecincts = new Hashtable();

    ////////// Registered and Casted Ballots by Gender;

    public Hashtable femalePopulationofPrecinct2021 = new Hashtable();
    public Hashtable femalePopulationofPrecinct2020 = new Hashtable();
    public Hashtable femalePopulationofPrecinct2018 = new Hashtable();
    public Hashtable femalePopulationofPrecinct2012 = new Hashtable();
    public Hashtable femalePopulationofPrecinct2016 = new Hashtable();

    public Hashtable malePopulationofPrecinct2021 = new Hashtable();
    public Hashtable malePopulationofPrecinct2020 = new Hashtable();
    public Hashtable malePopulationofPrecinct2018 = new Hashtable();
    public Hashtable malePopulationofPrecinct2016 = new Hashtable();
    public Hashtable malePopulationofPrecinct2012 = new Hashtable();

    public Hashtable femaleCastedBallotsInPrecinct2021 = new Hashtable();
    public Hashtable femaleCastedBallotsInPrecinct2020 = new Hashtable();
    public Hashtable femaleCastedBallotsInPrecinct2018 = new Hashtable();
    public Hashtable femaleCastedBallotsInPrecinct2016 = new Hashtable();
    public Hashtable femaleCastedBallotsInPrecinct2012 = new Hashtable();

    public Hashtable maleCastedBallotsInPrecinct2021 = new Hashtable();
    public Hashtable maleCastedBallotsInPrecinct2020 = new Hashtable();
    public Hashtable maleCastedBallotsInPrecinct2018 = new Hashtable();
    public Hashtable maleCastedBallotsInPrecinct2016 = new Hashtable();
    public Hashtable maleCastedBallotsInPrecinct2012 = new Hashtable();

    public Hashtable femalePercentageOfPrecinct2021 = new Hashtable();
    public Hashtable femalePercentageOfPrecinct2020 = new Hashtable();
    public Hashtable femalePercentageOfPrecinct2018 = new Hashtable();
    public Hashtable femalePercentageOfPrecinct2012 = new Hashtable();
    public Hashtable femalePercentageOfPrecinct2016 = new Hashtable();

    public Hashtable malePercentageOfPrecinct2021 = new Hashtable();
    public Hashtable malePercentageOfPrecinct2020 = new Hashtable();
    public Hashtable malePercentageOfPrecinct2018 = new Hashtable();
    public Hashtable malePercentageOfPrecinct2016 = new Hashtable();
    public Hashtable malePercentageOfPrecinct2012 = new Hashtable();


    ///// PVIs

    public Hashtable republicanPVI = new Hashtable();
    public Hashtable democraticPVI = new Hashtable();

    public Hashtable changeInRepublicanPVI = new Hashtable();
    public Hashtable changeInRepublicanPVINeg = new Hashtable();
    public Hashtable changeInDemocraticPVI = new Hashtable();
    public Hashtable changeInDemocraticPVINeg = new Hashtable();
    public Hashtable changeInPVIDemFlip = new Hashtable();
    public Hashtable changeInPVIRepFlip = new Hashtable();

    public Hashtable flippedDemPVI = new Hashtable();
    public Hashtable flippedRepPVI = new Hashtable();

    public Hashtable democraticPVIfor2016 = new Hashtable();
    public Hashtable democraticPVIfor2018 = new Hashtable();
    public Hashtable democraticPVIfor2020 = new Hashtable();
    public Hashtable democraticPVIfor2021 = new Hashtable();

    public Hashtable republicanPVIfor2016 = new Hashtable();
    public Hashtable republicanPVIfor2018 = new Hashtable();
    public Hashtable republicanPVIfor2020 = new Hashtable();
    public Hashtable republicanPVIfor2021 = new Hashtable();

    public Hashtable changeInMarginRepublican = new Hashtable();
    public Hashtable changeInMarginDemocratic = new Hashtable();
    public Hashtable changeInMarginBoth = new Hashtable();
    public Hashtable changeInCastedBallots = new Hashtable();
    public Hashtable changeInTurnout = new Hashtable();







    ///////////// Display Stuff

    public Hashtable marginOfWinDemocratWin = new Hashtable();

    public Hashtable marginOfWinRepublicanWin= new Hashtable();

    public Hashtable tiedPrecinct = new Hashtable();

    public Hashtable percentageDemocratic = new Hashtable();

    public Hashtable percentageRepublican = new Hashtable();

    public Hashtable demoChangeOverTimePos = new Hashtable();
    public Hashtable demoChangeOverTimeNeg = new Hashtable();

    public Hashtable changeinCastedBalPercentageTable = new Hashtable();

    public Hashtable castedBallotsInPrecinctDisplay = new Hashtable();

    ///////////// TURNOUT

    public Hashtable turnoutPercentage = new Hashtable();

    public Hashtable castedBallots = new Hashtable();

    ///////////// Split Precicnts

    public List<string> listOfSplitPrecincts = new List<string>();




    ///// Demo Stuff

    private float whitePopulationOfPrecinctFloat;
    private float blackPopulationOfPrecinctFloat;
    private float asianPopulationOfPrecinctFloat;
    private float hispanicPopulationOfPrecinctFloat;
    private float totalPopulationOfPrecinctFloat;

    private float whiteVotingPercentage;
    private float blackVotingPercentage;
    private float asianVotingPercentage;
    private float hispanicVotingPercentage;

    private float malePercentageOfPrecinct;
    private float femalePercentageOfPrecinct;
    private float malePopOfPrecinct;
    private float femalePopOfPrecinct;




    // Start is called before the first frame update
    void Start()
    {

    ///////////////// Split precincts, add them here so that all of the game objects get colored, not just one


    listOfSplitPrecincts.Add("Cobb, Austell 1A");
    listOfSplitPrecincts.Add("Cobb, Acworth 1A");
    listOfSplitPrecincts.Add("Cobb, North Cobb 01");
    listOfSplitPrecincts.Add("Cobb, Mars Hill 01");
    listOfSplitPrecincts.Add("Cobb, Acworth 1B");
    listOfSplitPrecincts.Add("Cobb, Baker 01");
    listOfSplitPrecincts.Add("Cobb, Cheatham Hill 02");
    listOfSplitPrecincts.Add("Cobb, Big Shanty 01");
    listOfSplitPrecincts.Add("Cobb, Elizabeth 01");
    listOfSplitPrecincts.Add("Cobb, Acworth 1C");
    listOfSplitPrecincts.Add("Cobb, Powder Springs 1A");
    listOfSplitPrecincts.Add("Cobb, Dobbins 01");
    listOfSplitPrecincts.Add("Cobb, Clarkdale 02");
    listOfSplitPrecincts.Add("Cobb, McEachern 01");
    listOfSplitPrecincts.Add("Cobb, Clarkdale 01");
    listOfSplitPrecincts.Add("Cobb, Mableton 02");
    listOfSplitPrecincts.Add("Cobb, Mableton 03");
    listOfSplitPrecincts.Add("Cobb, Lindley 01");
    listOfSplitPrecincts.Add("Cobb, Smyrna 3A");
    listOfSplitPrecincts.Add("Cobb, Smyrna 4A");
    listOfSplitPrecincts.Add("Cobb, Oregon 03");
    listOfSplitPrecincts.Add("Cobb, Kennesaw 1A");
    listOfSplitPrecincts.Add("Cobb, Kennesaw 2A");
    listOfSplitPrecincts.Add("Cobb, Kennesaw 3A");
    listOfSplitPrecincts.Add("Cobb, Kennesaw 4A");
    listOfSplitPrecincts.Add("Cobb, Pine Mountain 01");
    listOfSplitPrecincts.Add("Cobb, Pine Mountain 02");
    listOfSplitPrecincts.Add("Cobb, Marietta 5A");
    listOfSplitPrecincts.Add("Cobb, Marietta 7A");
    listOfSplitPrecincts.Add("Cobb, Marietta 3A");
    listOfSplitPrecincts.Add("Cobb, Marietta 1A");
    listOfSplitPrecincts.Add("Cobb, Marietta 4C");
    listOfSplitPrecincts.Add("Cobb, Bells Ferry 03");
    listOfSplitPrecincts.Add("Cobb, Lassiter 01");
    listOfSplitPrecincts.Add("Cobb, Marietta 4B");
    listOfSplitPrecincts.Add("Cobb, Marietta 6B");
    listOfSplitPrecincts.Add("Cobb, Elizabeth 04");
    listOfSplitPrecincts.Add("Cobb, Sewell Mill 03");
    listOfSplitPrecincts.Add("Cobb, Powers Ferry 01");
    listOfSplitPrecincts.Add("Cobb, Smyrna 1A");
    listOfSplitPrecincts.Add("Cobb, Oakdale 01");
    listOfSplitPrecincts.Add("Cobb, Nickajack 01");

    listOfSplitPrecincts.Add("Clayton, Jonesboro 13");
    listOfSplitPrecincts.Add("Clayton, Jonesboro 17");
    listOfSplitPrecincts.Add("Clayton, Riverdale 8");

    listOfSplitPrecincts.Add("Fulton, SC14A");
    listOfSplitPrecincts.Add("Fulton, SC16A");
    listOfSplitPrecincts.Add("Fulton, 09K");
    listOfSplitPrecincts.Add("Fulton, 02L1");
    listOfSplitPrecincts.Add("Fulton, 12A");
    listOfSplitPrecincts.Add("Fulton, 11M");
    listOfSplitPrecincts.Add("Fulton, 11B");
    listOfSplitPrecincts.Add("Fulton, EP04A");
    listOfSplitPrecincts.Add("Fulton, SC07A");
    listOfSplitPrecincts.Add("Fulton, UC02A");
    listOfSplitPrecincts.Add("Fulton, SC01A");
    listOfSplitPrecincts.Add("Fulton, RW12");
    listOfSplitPrecincts.Add("Fulton, ML03");
    listOfSplitPrecincts.Add("Fulton, JC04A");
    listOfSplitPrecincts.Add("Fulton, JC01");

    listOfSplitPrecincts.Add("DeKalb, Woodrow Road");
    listOfSplitPrecincts.Add("DeKalb, Redan Middle");

    //////////////////////////////////////////////////////////////////////////////////////////////////
    /////
    ///// GENDER
    /////
    //////////////////////////////////////////////////////////////////////////////////////////////////


    foreach(DictionaryEntry genderOfPrecinctCalc in malePopulationofPrecinct2021)
      {
        malePopOfPrecinct = (float)malePopulationofPrecinct2021[genderOfPrecinctCalc.Key];

        femalePopOfPrecinct = (float)femalePopulationofPrecinct2021[genderOfPrecinctCalc.Key];

        totalPopulationOfPrecinctFloat = femalePopOfPrecinct + malePopOfPrecinct;

        malePercentageOfPrecinct = malePopOfPrecinct / totalPopulationOfPrecinctFloat;

        malePercentageOfPrecinct *= 100;

        femalePercentageOfPrecinct = femalePopOfPrecinct / totalPopulationOfPrecinctFloat;

        femalePercentageOfPrecinct *= 100;

        malePercentageOfPrecinct2021.Add("" + genderOfPrecinctCalc.Key, malePercentageOfPrecinct);

        femalePercentageOfPrecinct2021.Add("" + genderOfPrecinctCalc.Key, femalePercentageOfPrecinct);
      }



    foreach(DictionaryEntry genderOfPrecinctCalc in malePopulationofPrecinct2020)
      {
        malePopOfPrecinct = (float)malePopulationofPrecinct2020[genderOfPrecinctCalc.Key];

        femalePopOfPrecinct = (float)femalePopulationofPrecinct2020[genderOfPrecinctCalc.Key];

        totalPopulationOfPrecinctFloat = femalePopOfPrecinct + malePopOfPrecinct;

        malePercentageOfPrecinct = malePopOfPrecinct / totalPopulationOfPrecinctFloat;

        malePercentageOfPrecinct *= 100;

        femalePercentageOfPrecinct = femalePopOfPrecinct / totalPopulationOfPrecinctFloat;

        femalePercentageOfPrecinct *= 100;

        malePercentageOfPrecinct2020.Add("" + genderOfPrecinctCalc.Key, malePercentageOfPrecinct);

        femalePercentageOfPrecinct2020.Add("" + genderOfPrecinctCalc.Key, femalePercentageOfPrecinct);
      }



    foreach(DictionaryEntry genderOfPrecinctCalc in malePopulationofPrecinct2018)
      {
        malePopOfPrecinct = (float)malePopulationofPrecinct2018[genderOfPrecinctCalc.Key];

        femalePopOfPrecinct = (float)femalePopulationofPrecinct2018[genderOfPrecinctCalc.Key];

        totalPopulationOfPrecinctFloat = femalePopOfPrecinct + malePopOfPrecinct;

        malePercentageOfPrecinct = malePopOfPrecinct / totalPopulationOfPrecinctFloat;

        malePercentageOfPrecinct *= 100;

        femalePercentageOfPrecinct = femalePopOfPrecinct / totalPopulationOfPrecinctFloat;

        femalePercentageOfPrecinct *= 100;

        malePercentageOfPrecinct2018.Add("" + genderOfPrecinctCalc.Key, malePercentageOfPrecinct);

        femalePercentageOfPrecinct2018.Add("" + genderOfPrecinctCalc.Key, femalePercentageOfPrecinct);

        //Debug.Log("" + malePercentageOfPrecinct + " this is the Male pop percantage in " + genderOfPrecinctCalc.Key + ".");
      }



    foreach(DictionaryEntry genderOfPrecinctCalc in malePopulationofPrecinct2016)
      {
        malePopOfPrecinct = (float)malePopulationofPrecinct2016[genderOfPrecinctCalc.Key];

        femalePopOfPrecinct = (float)femalePopulationofPrecinct2016[genderOfPrecinctCalc.Key];

        totalPopulationOfPrecinctFloat = femalePopOfPrecinct + malePopOfPrecinct;

        malePercentageOfPrecinct = malePopOfPrecinct / totalPopulationOfPrecinctFloat;

        malePercentageOfPrecinct *= 100;

        femalePercentageOfPrecinct = femalePopOfPrecinct / totalPopulationOfPrecinctFloat;

        femalePercentageOfPrecinct *= 100;

        malePercentageOfPrecinct2016.Add("" + genderOfPrecinctCalc.Key, malePercentageOfPrecinct);

        femalePercentageOfPrecinct2016.Add("" + genderOfPrecinctCalc.Key, femalePercentageOfPrecinct);
      }



    foreach(DictionaryEntry genderOfPrecinctCalc in malePopulationofPrecinct2012)
      {
        malePopOfPrecinct = (float)malePopulationofPrecinct2012[genderOfPrecinctCalc.Key];

        femalePopOfPrecinct = (float)femalePopulationofPrecinct2012[genderOfPrecinctCalc.Key];

        totalPopulationOfPrecinctFloat = femalePopOfPrecinct + malePopOfPrecinct;

        malePercentageOfPrecinct = malePopOfPrecinct / totalPopulationOfPrecinctFloat;

        malePercentageOfPrecinct *= 100;

        femalePercentageOfPrecinct = femalePopOfPrecinct / totalPopulationOfPrecinctFloat;

        femalePercentageOfPrecinct *= 100;

        malePercentageOfPrecinct2012.Add("" + genderOfPrecinctCalc.Key, malePercentageOfPrecinct);

        femalePercentageOfPrecinct2012.Add("" + genderOfPrecinctCalc.Key, femalePercentageOfPrecinct);
      }






    //////////////////////////////////////////////////////////////////////////////////////////////////
    /////
    ///// RACE
    /////
    //////////////////////////////////////////////////////////////////////////////////////////////////


    foreach(DictionaryEntry populationOfPrecinctCalc in whiteRegisteredVotersInPrecinct2021)
      {

        whitePopulationOfPrecinctFloat = (float)whiteRegisteredVotersInPrecinct2021[populationOfPrecinctCalc.Key];

        if(blackRegisteredVotersInPrecinct2021.ContainsKey(populationOfPrecinctCalc.Key))
          {

            blackPopulationOfPrecinctFloat = (float)blackRegisteredVotersInPrecinct2021[populationOfPrecinctCalc.Key];

            asianPopulationOfPrecinctFloat = (float)asianRegisteredVotersInPrecinct2021[populationOfPrecinctCalc.Key];

            hispanicPopulationOfPrecinctFloat = (float)hispanicRegisteredVotersInPrecinct2021[populationOfPrecinctCalc.Key];

            totalPopulationOfPrecinctFloat = whitePopulationOfPrecinctFloat + blackPopulationOfPrecinctFloat + asianPopulationOfPrecinctFloat + hispanicPopulationOfPrecinctFloat;



            whiteVotingPercentage = whitePopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            whiteVotingPercentage = whiteVotingPercentage * 100;

            blackVotingPercentage = blackPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            blackVotingPercentage = blackVotingPercentage * 100;

            asianVotingPercentage = asianPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            asianVotingPercentage = asianVotingPercentage  * 100;

            hispanicVotingPercentage = hispanicPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            hispanicVotingPercentage = hispanicVotingPercentage * 100;

        //    Debug.Log("Percentages for each precinct:White Voters: " + whiteVotingPercentage + "%. Black Voters: " + blackVotingPercentage + "%. Asian Voters: " + asianVotingPercentage + "%. ");



            whitePopulationOfPrecinct2021.Add("" + populationOfPrecinctCalc.Key, whiteVotingPercentage);

            blackPopulationOfPrecinct2021.Add("" + populationOfPrecinctCalc.Key, blackVotingPercentage);

            asianPopulationOfPrecinct2021.Add("" + populationOfPrecinctCalc.Key, asianVotingPercentage);

            hispanicPopulationOfPrecinct2021.Add("" + populationOfPrecinctCalc.Key, hispanicVotingPercentage);
          }
      }


    foreach(DictionaryEntry populationOfPrecinctCalc in whiteRegisteredVotersInPrecinct2020)
      {

        whitePopulationOfPrecinctFloat = (float)whiteRegisteredVotersInPrecinct2020[populationOfPrecinctCalc.Key];

        if(blackRegisteredVotersInPrecinct2020.ContainsKey(populationOfPrecinctCalc.Key))
          {

            blackPopulationOfPrecinctFloat = (float)blackRegisteredVotersInPrecinct2020[populationOfPrecinctCalc.Key];

            asianPopulationOfPrecinctFloat = (float)asianRegisteredVotersInPrecinct2020[populationOfPrecinctCalc.Key];

            hispanicPopulationOfPrecinctFloat = (float)hispanicRegisteredVotersInPrecinct2020[populationOfPrecinctCalc.Key];

            totalPopulationOfPrecinctFloat = whitePopulationOfPrecinctFloat + blackPopulationOfPrecinctFloat + asianPopulationOfPrecinctFloat + hispanicPopulationOfPrecinctFloat;



            whiteVotingPercentage = whitePopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            whiteVotingPercentage = whiteVotingPercentage * 100;

            blackVotingPercentage = blackPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            blackVotingPercentage = blackVotingPercentage * 100;

            asianVotingPercentage = asianPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            asianVotingPercentage = asianVotingPercentage  * 100;

            hispanicVotingPercentage = hispanicPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            hispanicVotingPercentage = hispanicVotingPercentage * 100;

        //    Debug.Log("Percentages for each precinct:White Voters: " + whiteVotingPercentage + "%. Black Voters: " + blackVotingPercentage + "%. Asian Voters: " + asianVotingPercentage + "%. ");



            whitePopulationOfPrecinct2020.Add("" + populationOfPrecinctCalc.Key, whiteVotingPercentage);

            blackPopulationOfPrecinct2020.Add("" + populationOfPrecinctCalc.Key, blackVotingPercentage);

            asianPopulationOfPrecinct2020.Add("" + populationOfPrecinctCalc.Key, asianVotingPercentage);

            hispanicPopulationOfPrecinct2020.Add("" + populationOfPrecinctCalc.Key, hispanicVotingPercentage);
          }
      }



    foreach(DictionaryEntry populationOfPrecinctCalc in whiteRegisteredVotersInPrecinct2018)
      {

        whitePopulationOfPrecinctFloat = (float)whiteRegisteredVotersInPrecinct2018[populationOfPrecinctCalc.Key];

        if(blackRegisteredVotersInPrecinct2018.ContainsKey(populationOfPrecinctCalc.Key))
          {

            blackPopulationOfPrecinctFloat = (float)blackRegisteredVotersInPrecinct2018[populationOfPrecinctCalc.Key];

            asianPopulationOfPrecinctFloat = (float)asianRegisteredVotersInPrecinct2018[populationOfPrecinctCalc.Key];

            hispanicPopulationOfPrecinctFloat = (float)hispanicRegisteredVotersInPrecinct2018[populationOfPrecinctCalc.Key];

            totalPopulationOfPrecinctFloat = whitePopulationOfPrecinctFloat + blackPopulationOfPrecinctFloat + asianPopulationOfPrecinctFloat + hispanicPopulationOfPrecinctFloat;



            whiteVotingPercentage = whitePopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            whiteVotingPercentage = whiteVotingPercentage * 100;

            blackVotingPercentage = blackPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            blackVotingPercentage = blackVotingPercentage * 100;

            asianVotingPercentage = asianPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            asianVotingPercentage = asianVotingPercentage  * 100;

            hispanicVotingPercentage = hispanicPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            hispanicVotingPercentage = hispanicVotingPercentage * 100;

        //    Debug.Log("Percentages for each precinct:White Voters: " + whiteVotingPercentage + "%. Black Voters: " + blackVotingPercentage + "%. Asian Voters: " + asianVotingPercentage + "%. ");



            whitePopulationOfPrecinct2018.Add("" + populationOfPrecinctCalc.Key, whiteVotingPercentage);

            blackPopulationOfPrecinct2018.Add("" + populationOfPrecinctCalc.Key, blackVotingPercentage);

            asianPopulationOfPrecinct2018.Add("" + populationOfPrecinctCalc.Key, asianVotingPercentage);

            hispanicPopulationOfPrecinct2018.Add("" + populationOfPrecinctCalc.Key, hispanicVotingPercentage);
          }
      }



    foreach(DictionaryEntry populationOfPrecinctCalc in whiteRegisteredVotersInPrecinct2016)
      {

        whitePopulationOfPrecinctFloat = (float)whiteRegisteredVotersInPrecinct2016[populationOfPrecinctCalc.Key];

        if(blackRegisteredVotersInPrecinct2016.ContainsKey(populationOfPrecinctCalc.Key))
          {

            blackPopulationOfPrecinctFloat = (float)blackRegisteredVotersInPrecinct2016[populationOfPrecinctCalc.Key];

            asianPopulationOfPrecinctFloat = (float)asianRegisteredVotersInPrecinct2016[populationOfPrecinctCalc.Key];

            hispanicPopulationOfPrecinctFloat = (float)hispanicRegisteredVotersInPrecinct2016[populationOfPrecinctCalc.Key];

            totalPopulationOfPrecinctFloat = whitePopulationOfPrecinctFloat + blackPopulationOfPrecinctFloat + asianPopulationOfPrecinctFloat + hispanicPopulationOfPrecinctFloat;



            whiteVotingPercentage = whitePopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            whiteVotingPercentage = whiteVotingPercentage * 100;

            blackVotingPercentage = blackPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            blackVotingPercentage = blackVotingPercentage * 100;

            asianVotingPercentage = asianPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            asianVotingPercentage = asianVotingPercentage  * 100;

            hispanicVotingPercentage = hispanicPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            hispanicVotingPercentage = hispanicVotingPercentage * 100;

        //    Debug.Log("Percentages for each precinct:White Voters: " + whiteVotingPercentage + "%. Black Voters: " + blackVotingPercentage + "%. Asian Voters: " + asianVotingPercentage + "%. ");



            whitePopulationOfPrecinct2016.Add("" + populationOfPrecinctCalc.Key, whiteVotingPercentage);

            blackPopulationOfPrecinct2016.Add("" + populationOfPrecinctCalc.Key, blackVotingPercentage);

            asianPopulationOfPrecinct2016.Add("" + populationOfPrecinctCalc.Key, asianVotingPercentage);

            hispanicPopulationOfPrecinct2016.Add("" + populationOfPrecinctCalc.Key, hispanicVotingPercentage);
          }
      }


    foreach(DictionaryEntry populationOfPrecinctCalc in whiteRegisteredVotersInPrecinct2012)
      {

        whitePopulationOfPrecinctFloat = (float)whiteRegisteredVotersInPrecinct2012[populationOfPrecinctCalc.Key];

        if(blackRegisteredVotersInPrecinct2012.ContainsKey(populationOfPrecinctCalc.Key))
          {

            blackPopulationOfPrecinctFloat = (float)blackRegisteredVotersInPrecinct2012[populationOfPrecinctCalc.Key];

            asianPopulationOfPrecinctFloat = (float)asianRegisteredVotersInPrecinct2012[populationOfPrecinctCalc.Key];

            hispanicPopulationOfPrecinctFloat = (float)hispanicRegisteredVotersInPrecinct2012[populationOfPrecinctCalc.Key];

            totalPopulationOfPrecinctFloat = whitePopulationOfPrecinctFloat + blackPopulationOfPrecinctFloat + asianPopulationOfPrecinctFloat + hispanicPopulationOfPrecinctFloat;



            whiteVotingPercentage = whitePopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            whiteVotingPercentage = whiteVotingPercentage * 100;

            blackVotingPercentage = blackPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            blackVotingPercentage = blackVotingPercentage * 100;

            asianVotingPercentage = asianPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            asianVotingPercentage = asianVotingPercentage  * 100;

            hispanicVotingPercentage = hispanicPopulationOfPrecinctFloat / totalPopulationOfPrecinctFloat;

            hispanicVotingPercentage = hispanicVotingPercentage * 100;

        //    Debug.Log("Percentages for each precinct:White Voters: " + whiteVotingPercentage + "%. Black Voters: " + blackVotingPercentage + "%. Asian Voters: " + asianVotingPercentage + "%. ");



            whitePopulationOfPrecinct2012.Add("" + populationOfPrecinctCalc.Key, whiteVotingPercentage);

            blackPopulationOfPrecinct2012.Add("" + populationOfPrecinctCalc.Key, blackVotingPercentage);

            asianPopulationOfPrecinct2012.Add("" + populationOfPrecinctCalc.Key, asianVotingPercentage);

            hispanicPopulationOfPrecinct2012.Add("" + populationOfPrecinctCalc.Key, hispanicVotingPercentage);
          }
      }




      ///////////////////////////////////////////////////////////////////////////////////
      ////////////////
      //////////////// PVI
      ////////////////
      ///////////////////////////////////////////////////////////////////////////////////


      foreach(DictionaryEntry pviOfPrecinct in democratVotes2012) ///// PVI For 2016
        {
          float precinctResultsYearOneDem = (float)pviOfPrecinct.Value;

          if(democratVotes2016.ContainsKey(pviOfPrecinct.Key))
            {
              float precinctResultsYearTwoDem = (float)democratVotes2016[pviOfPrecinct.Key];

              float precinctResultsYearOneRep = (float)republicanVotes2012[pviOfPrecinct.Key];

              float precinctResultsYearTwoRep = (float)republicanVotes2016[pviOfPrecinct.Key];


              float totalResultsYearOne = (float)castedBallotsInPrecinct2012[pviOfPrecinct.Key];

              float totalResultsYearTwo = (float)castedBallotsInPrecinct2016[pviOfPrecinct.Key];

              //Debug.Log("First Results Dem " + precinctResultsYearOneDem + ". Second: " + precinctResultsYearTwoDem + ". TotalYr1: " + totalResultsYearOne);

              float demPercentageYearOne = precinctResultsYearOneDem / totalResultsYearOne;

              float demPercentageYearTwo = precinctResultsYearTwoDem / totalResultsYearTwo;

              float repPercentageYearOne = precinctResultsYearOneRep / totalResultsYearOne;

              float repPercentageYearTwo = precinctResultsYearTwoRep / totalResultsYearTwo;

              //Debug.Log("First Dem Percentage: " + demPercentageYearOne + ". Second Dem percentage: " + demPercentageYearTwo);

              float averageResultsDem = demPercentageYearOne + demPercentageYearTwo;


              averageResultsDem /= 2;

            //  Debug.Log("Average Results Dem: " + averageResultsDem +". Average Statewide: " + averageStatewideResultsDemocrat);

              averageResultsDem -= 0.45415f;

              averageResultsDem *= 100;

              if(averageResultsDem < 0) /// If it's positive it means it's a +D PVI district. Otherwise, run it with Republcians
                {

                  float averageResultsRep = repPercentageYearOne + repPercentageYearTwo;

                  averageResultsRep /= 2;

                  averageResultsRep -= 0.49315f;

                  averageResultsRep *= 100;

                  republicanPVIfor2016.Add("" + pviOfPrecinct.Key, averageResultsRep);

            //      Debug.Log("PVI for " + pviOfPrecinct.Key + " is R+" + averageResultsRep);
                }

              else if(averageResultsDem > 0)
                {
                  democraticPVIfor2016.Add("" + pviOfPrecinct.Key, averageResultsDem);

              //    Debug.Log("PVI for " + pviOfPrecinct.Key + " is D+" + averageResultsDem);
                }
            }

        }



      foreach(DictionaryEntry pviOfPrecinct in democratVotes2016) ///// PVI For 2018
        {
          float precinctResultsYearOneDem = (float)pviOfPrecinct.Value;

          if(democratVotes2018.ContainsKey(pviOfPrecinct.Key))
            {
              float precinctResultsYearTwoDem = (float)democratVotes2018[pviOfPrecinct.Key];

              float precinctResultsYearOneRep = (float)republicanVotes2016[pviOfPrecinct.Key];

              float precinctResultsYearTwoRep = (float)republicanVotes2018[pviOfPrecinct.Key];


              float totalResultsYearOne = (float)castedBallotsInPrecinct2016[pviOfPrecinct.Key];

              float totalResultsYearTwo = (float)castedBallotsInPrecinct2018[pviOfPrecinct.Key];

              //Debug.Log("First Results Dem " + precinctResultsYearOneDem + ". Second: " + precinctResultsYearTwoDem + ". TotalYr1: " + totalResultsYearOne);

              float demPercentageYearOne = precinctResultsYearOneDem / totalResultsYearOne;

              float demPercentageYearTwo = precinctResultsYearTwoDem / totalResultsYearTwo;

              float repPercentageYearOne = precinctResultsYearOneRep / totalResultsYearOne;

              float repPercentageYearTwo = precinctResultsYearTwoRep / totalResultsYearTwo;

              //Debug.Log("First Dem Percentage: " + demPercentageYearOne + ". Second Dem percentage: " + demPercentageYearTwo);

              float averageResultsDem = demPercentageYearOne + demPercentageYearTwo;


              averageResultsDem /= 2;

            //  Debug.Log("Average Results Dem: " + averageResultsDem +". Average Statewide: " + averageStatewideResultsDemocrat);

              averageResultsDem -= 0.47415f;

              averageResultsDem *= 100;

              if(averageResultsDem < 0) /// If it's positive it means it's a +D PVI district. Otherwise, run it with Republcians
                {

                  float averageResultsRep = repPercentageYearOne + repPercentageYearTwo;

                  averageResultsRep /= 2;

                  averageResultsRep -= 0.5032f;

                  averageResultsRep *= 100;

                  republicanPVIfor2018.Add("" + pviOfPrecinct.Key, averageResultsRep);

              //    Debug.Log("PVI for " + pviOfPrecinct.Key + " is R+" + averageResultsRep);
                }

              else if(averageResultsDem > 0)
                {
                  democraticPVIfor2018.Add("" + pviOfPrecinct.Key, averageResultsDem);

                //  Debug.Log("PVI for " + pviOfPrecinct.Key + " is D+" + averageResultsDem);
                }
            }

        }



      foreach(DictionaryEntry pviOfPrecinct in democratVotes2018) ///// PVI For 2020
        {
          float precinctResultsYearOneDem = (float)pviOfPrecinct.Value;

          if(democratVotes2020.ContainsKey(pviOfPrecinct.Key))
            {
              float precinctResultsYearTwoDem = (float)democratVotes2020[pviOfPrecinct.Key];

              float precinctResultsYearOneRep = (float)republicanVotes2018[pviOfPrecinct.Key];

              float precinctResultsYearTwoRep = (float)republicanVotes2020[pviOfPrecinct.Key];


              float totalResultsYearOne = (float)castedBallotsInPrecinct2018[pviOfPrecinct.Key];

              float totalResultsYearTwo = (float)castedBallotsInPrecinct2020[pviOfPrecinct.Key];

              //Debug.Log("First Results Dem " + precinctResultsYearOneDem + ". Second: " + precinctResultsYearTwoDem + ". TotalYr1: " + totalResultsYearOne);

              float demPercentageYearOne = precinctResultsYearOneDem / totalResultsYearOne;

              float demPercentageYearTwo = precinctResultsYearTwoDem / totalResultsYearTwo;

              float repPercentageYearOne = precinctResultsYearOneRep / totalResultsYearOne;

              float repPercentageYearTwo = precinctResultsYearTwoRep / totalResultsYearTwo;

              //Debug.Log("First Dem Percentage: " + demPercentageYearOne + ". Second Dem percentage: " + demPercentageYearTwo);

              float averageResultsDem = demPercentageYearOne + demPercentageYearTwo;


              averageResultsDem /= 2;

            //  Debug.Log("Average Results Dem: " + averageResultsDem +". Average Statewide: " + averageStatewideResultsDemocrat);

              averageResultsDem -= 0.49135f;

              averageResultsDem *= 100;

              if(averageResultsDem < 0) /// If it's positive it means it's a +D PVI district. Otherwise, run it with Republcians
                {

                  float averageResultsRep = repPercentageYearOne + repPercentageYearTwo;

                  averageResultsRep /= 2;

                  averageResultsRep -= 0.4972f;

                  averageResultsRep *= 100;

                  republicanPVIfor2020.Add("" + pviOfPrecinct.Key, averageResultsRep);

              //    Debug.Log("PVI for " + pviOfPrecinct.Key + " is R+" + averageResultsRep);
                }

              else if(averageResultsDem > 0)
                {
                  democraticPVIfor2020.Add("" + pviOfPrecinct.Key, averageResultsDem);

              //    Debug.Log("PVI for " + pviOfPrecinct.Key + " is D+" + averageResultsDem);
                }
            }

        }



      foreach(DictionaryEntry pviOfPrecinct in democratVotes2020) ///// PVI For 2021
        {
          float precinctResultsYearOneDem = (float)pviOfPrecinct.Value;

          if(democratVotes2021.ContainsKey(pviOfPrecinct.Key))
            {
              float precinctResultsYearTwoDem = (float)democratVotes2021[pviOfPrecinct.Key];

              float precinctResultsYearOneRep = (float)republicanVotes2020[pviOfPrecinct.Key];

              float precinctResultsYearTwoRep = (float)republicanVotes2021[pviOfPrecinct.Key];


              float totalResultsYearOne = (float)castedBallotsInPrecinct2020[pviOfPrecinct.Key];

              float totalResultsYearTwo = (float)castedBallotsInPrecinct2021[pviOfPrecinct.Key];

              //Debug.Log("First Results Dem " + precinctResultsYearOneDem + ". Second: " + precinctResultsYearTwoDem + ". TotalYr1: " + totalResultsYearOne);

              float demPercentageYearOne = precinctResultsYearOneDem / totalResultsYearOne;

              float demPercentageYearTwo = precinctResultsYearTwoDem / totalResultsYearTwo;

              float repPercentageYearOne = precinctResultsYearOneRep / totalResultsYearOne;

              float repPercentageYearTwo = precinctResultsYearTwoRep / totalResultsYearTwo;

              //Debug.Log("First Dem Percentage: " + demPercentageYearOne + ". Second Dem percentage: " + demPercentageYearTwo);

              float averageResultsDem = demPercentageYearOne + demPercentageYearTwo;


              averageResultsDem /= 2;

            //  Debug.Log("Average Results Dem: " + averageResultsDem +". Average Statewide: " + averageStatewideResultsDemocrat);

              averageResultsDem -= 0.5004f;

              averageResultsDem *= 100;

              if(averageResultsDem < 0) /// If it's positive it means it's a +D PVI district. Otherwise, run it with Republcians
                {

                  float averageResultsRep = repPercentageYearOne + repPercentageYearTwo;

                  averageResultsRep /= 2;

                  averageResultsRep -= 0.49315f;

                  averageResultsRep *= 100;

                  republicanPVIfor2021.Add("" + pviOfPrecinct.Key, averageResultsRep);

              //    Debug.Log("PVI for " + pviOfPrecinct.Key + " is R+" + averageResultsRep);
                }

              else if(averageResultsDem > 0)
                {
                  democraticPVIfor2021.Add("" + pviOfPrecinct.Key, averageResultsDem);

              //    Debug.Log("PVI for " + pviOfPrecinct.Key + " is D+" + averageResultsDem);
                }
            }

        }

    }
}
