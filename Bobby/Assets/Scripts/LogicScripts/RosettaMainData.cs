﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class RosettaMainData : MonoBehaviour
{

  private float yearSelected;
  private string countyName;
  private string precinctName;
  private float republicanVotes;
  private float democratVotes;
  private float castedBallots;
  private float registeredVoters;

  private TextAsset[] dataStorage;

  private string finalString;

    // Start is called before the first frame update
    void Awake()
    {

        ////////////////////////////////////////////////
        /////// All the TextAsset files go here
        ///////////////////////////////////////////////

        TextAsset cobb2021Data = Resources.Load<TextAsset>("Cobb/CobbData2021");
        TextAsset cobb2020Data = Resources.Load<TextAsset>("Cobb/CobbData2020");
        TextAsset cobb2018Data = Resources.Load<TextAsset>("Cobb/CobbData2018"); /// Sets the textAssett equal to the file specified
        TextAsset cobb2016Data = Resources.Load<TextAsset>("Cobb/CobbData2016");
        TextAsset cobb2012Data = Resources.Load<TextAsset>("Cobb/CobbData2012");

        TextAsset cherokee2021Data = Resources.Load<TextAsset>("Cherokee/CherokeeData2021");
        TextAsset cherokee2020Data = Resources.Load<TextAsset>("Cherokee/CherokeeData2020");
        TextAsset cherokee2018Data = Resources.Load<TextAsset>("Cherokee/CherokeeData2018");
        TextAsset cherokee2016Data = Resources.Load<TextAsset>("Cherokee/CherokeeData2016");
        TextAsset cherokee2012Data = Resources.Load<TextAsset>("Cherokee/CherokeeData2012");

        TextAsset forsyth2021Data = Resources.Load<TextAsset>("Forsyth/ForsythData2021");
        TextAsset forsyth2020Data = Resources.Load<TextAsset>("Forsyth/ForsythData2020");
        TextAsset forsyth2018Data = Resources.Load<TextAsset>("Forsyth/ForsythData2018");
        TextAsset forsyth2016Data = Resources.Load<TextAsset>("Forsyth/ForsythData2016");
        TextAsset forsyth2012Data = Resources.Load<TextAsset>("Forsyth/ForsythData2012");

        TextAsset clayton2021Data = Resources.Load<TextAsset>("Clayton/ClaytonData2021");
        TextAsset clayton2020Data = Resources.Load<TextAsset>("Clayton/ClaytonData2020");
        TextAsset clayton2018Data = Resources.Load<TextAsset>("Clayton/ClaytonData2018");
        TextAsset clayton2016Data = Resources.Load<TextAsset>("Clayton/ClaytonData2016");
        TextAsset clayton2012Data = Resources.Load<TextAsset>("Clayton/ClaytonData2012");

        TextAsset fulton2021Data = Resources.Load<TextAsset>("Fulton/FultonData2021");
        TextAsset fulton2020Data = Resources.Load<TextAsset>("Fulton/FultonData2020");
        TextAsset fulton2018Data = Resources.Load<TextAsset>("Fulton/FultonData2018");
        TextAsset fulton2016Data = Resources.Load<TextAsset>("Fulton/FultonData2016");
        TextAsset fulton2012Data = Resources.Load<TextAsset>("Fulton/FultonData2012");

        TextAsset gwinnett2021Data = Resources.Load<TextAsset>("Gwinnett/GwinnettData2021");
        TextAsset gwinnett2020Data = Resources.Load<TextAsset>("Gwinnett/GwinnettData2020");
        TextAsset gwinnett2018Data = Resources.Load<TextAsset>("Gwinnett/GwinnettData2018");
        TextAsset gwinnett2016Data = Resources.Load<TextAsset>("Gwinnett/GwinnettData2016");
        TextAsset gwinnett2012Data = Resources.Load<TextAsset>("Gwinnett/GwinnettData2012");

        TextAsset dekalb2021Data = Resources.Load<TextAsset>("DeKalb/DeKalbData2021");
        TextAsset dekalb2020Data = Resources.Load<TextAsset>("DeKalb/DeKalbData2020");
        TextAsset dekalb2018Data = Resources.Load<TextAsset>("DeKalb/DeKalbData2018");
        TextAsset dekalb2016Data = Resources.Load<TextAsset>("DeKalb/DeKalbData2016");
        TextAsset dekalb2012Data = Resources.Load<TextAsset>("DeKalb/DeKalbData2012");


        ////////////// End

        TextAsset[] dataStorage = new TextAsset[50]; /// Initalizes the Array

        ////////////////////////////////////////////////
        /////// Adding the TextAssets to the Array
        ///////////////////////////////////////////////

        dataStorage[0] = cobb2012Data;
        dataStorage[1] = cobb2016Data;
        dataStorage[2] = cobb2018Data;
        dataStorage[3] = cobb2020Data;
        dataStorage[4] = cobb2021Data;
        dataStorage[5] = cherokee2012Data;
        dataStorage[6] = cherokee2016Data;
        dataStorage[7] = cherokee2018Data;
        dataStorage[8] = cherokee2020Data;
        dataStorage[9] = cherokee2021Data;
        dataStorage[10] = forsyth2012Data;
        dataStorage[11] = forsyth2016Data;
        dataStorage[12] = forsyth2018Data;
        dataStorage[13] = forsyth2020Data;
        dataStorage[14] = forsyth2021Data;
        dataStorage[15] = clayton2012Data;
        dataStorage[16] = clayton2016Data;
        dataStorage[17] = clayton2018Data;
        dataStorage[18] = clayton2020Data;
        dataStorage[19] = clayton2021Data;
        dataStorage[20] = fulton2012Data;
        dataStorage[21] = fulton2016Data;
        dataStorage[22] = fulton2018Data;
        dataStorage[23] = fulton2020Data;
        dataStorage[24] = fulton2021Data;
        dataStorage[25] = dekalb2012Data;
        dataStorage[26] = dekalb2016Data;
        dataStorage[27] = dekalb2018Data;
        dataStorage[28] = dekalb2020Data;
        dataStorage[29] = gwinnett2016Data;
        dataStorage[30] = gwinnett2018Data;
        dataStorage[31] = gwinnett2020Data;
        dataStorage[32] = gwinnett2012Data;
      //  dataStorage[33] = gwinnett2021Data;
      // dataStorage[34] = dekalb2021Data;

    //   Debug.Log("Working");


        ////////////// End

        foreach (TextAsset dataSheet in dataStorage) /// Goes through every TextAsset in the Array
          {
          //  Debug.Log("Working 2");

          //  Debug.Log(dataSheet);

            string [] demoData = dataSheet.text.Split(new char[] { '\n' }); /// Takes the data and removes any line breaks before putting it into a string array. First letter is 0.

          //  Debug.Log("Working 3");

            for(int i = 1; i < demoData.Length-1; i++) /// i = X is the row to start at, i < length means it goes to the end, and i++ means it just keeps doing it
              {

                string[] dataRows = demoData[i].Split(new char[] { ','}); /// Take out the commas

                yearSelected = float.Parse(dataRows[0]); /// The year covered by the data is in row 0

                countyName = dataRows[1]; /// The name of the county is on row 1

                precinctName = dataRows[2]; /// The name of the precinct is in row 2

                republicanVotes = float.Parse(dataRows[3]); // Rep voters are in row 3

                democratVotes = float.Parse(dataRows[4]);

                castedBallots = float.Parse(dataRows[5]);

                registeredVoters = float.Parse(dataRows[6]);

                finalString = "" +  countyName + ", " + precinctName; // Creates the final string by adding the county name and precinct name together

               Debug.Log("Year: " + yearSelected + ". Precinct: " + finalString);
            //  Debug.Log(yearSelected);

                if(yearSelected == 2021)
                  {
                //    Debug.Log("Currently on 2021");
                    gameObject.GetComponent<InitStartup>().republicanVotes2021.Add(finalString, republicanVotes);

                    gameObject.GetComponent<InitStartup>().democratVotes2021.Add(finalString, democratVotes);

                    gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2021.Add(finalString, castedBallots);

                    gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2021.Add(finalString, registeredVoters);
                  }


                  if(yearSelected == 2020)
                    {
                    //  Debug.Log("Currently on 2020");
                      gameObject.GetComponent<InitStartup>().republicanVotes2020.Add(finalString, republicanVotes);

                      gameObject.GetComponent<InitStartup>().democratVotes2020.Add(finalString, democratVotes);

                      gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2020.Add(finalString, castedBallots);

                      gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2020.Add(finalString, registeredVoters);
                    }

                    if(yearSelected == 2018)
                      {
                    //    Debug.Log("Currently on 2018");
                        gameObject.GetComponent<InitStartup>().republicanVotes2018.Add(finalString, republicanVotes);

                        gameObject.GetComponent<InitStartup>().democratVotes2018.Add(finalString, democratVotes);

                        gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2018.Add(finalString, castedBallots);

                        gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2018.Add(finalString, registeredVoters);
                      }

                      if(yearSelected == 2016)
                        {
                      //    Debug.Log("Currently on 2016");
                          gameObject.GetComponent<InitStartup>().republicanVotes2016.Add(finalString, republicanVotes);

                          gameObject.GetComponent<InitStartup>().democratVotes2016.Add(finalString, democratVotes);

                          gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2016.Add(finalString, castedBallots);

                          gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2016.Add(finalString, registeredVoters);
                        }

                        if(yearSelected == 2012)
                          {
                        //    Debug.Log("Currently on 2012");
                            gameObject.GetComponent<InitStartup>().republicanVotes2012.Add(finalString, republicanVotes);

                            gameObject.GetComponent<InitStartup>().democratVotes2012.Add(finalString, democratVotes);

                            gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2012.Add(finalString, castedBallots);

                            gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2012.Add(finalString, registeredVoters);
                          }

          //     Debug.Log("Currently on precinct " + precinctName);

              //  Debug.Log(finalString);



    // For Demo stuff  float.Parse(dataRows[7]) + float.Parse(dataRows[9]) + float.Parse(dataRows[11]);
              }
          }
    }
}
