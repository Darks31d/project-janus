﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemographicManager : MonoBehaviour
{


    //// Demographic Changes variables

    public float percentChangetoLookAt;

    private float whitePopulationLater;
    private float whitePopulationEarlier;
    private float whitePopulationGrowthStat;

    private float blackPopulationLater;
    private float blackPopulationEarlier;
    private float blackPopulationGrowthStat;

    private float asianPopulationLater;
    private float asianPopulationEarlier;
    private float asianPopulationGrowthStat;

    private float hispanicPopulationLater;
    private float hispanicPopulationEarlier;
    private float hispanicPopulationGrowthStat;



    //// Demographic Visualization Variables

    public float demographicVisSelection;
    private float whiteDemoVisRanking;
    private float asianDemoVisRanking;
    private float hispanicDemoVisRanking;
    private float largestDemographicinPrecinct;
    public List<float> demographicPopulationRanking;


    //////////////////////////////////////////
    //// NEW STUFF
    /////////////////////////////////////////

    public Hashtable dictionaryDemographicPopulationToLookAtYearOne = new Hashtable();

    public Hashtable dictionaryDemographicPopulationToLookAtYearTwo = new Hashtable();

    private GameObject precinctColorChangeGameObject;
    private GameObject[] precinctColorChangeGameObjectsForSplitPrecincts;
    private string precinctColorChangeGameObjectFinder;
    private bool currentPrecicntIsSplit;

    private float popOfDemoFloat;

    public float popVisColor1 = 1;
    public float popVisColor2 = 1;
    public float popVisColor3 = 1; 


    public void demoVisualizePopTwoYears()
      {

        foreach(DictionaryEntry percentofDemoInPrecinctOverTime in dictionaryDemographicPopulationToLookAtYearOne)
          {

              float popofDemoYearOneFloat = (float)dictionaryDemographicPopulationToLookAtYearOne[percentofDemoInPrecinctOverTime.Key];

              if(dictionaryDemographicPopulationToLookAtYearTwo.ContainsKey(percentofDemoInPrecinctOverTime.Key))
                {
                  Debug.Log("Working for" + percentofDemoInPrecinctOverTime.Key);
                  float popOfDemoYearTwoFloat = (float)dictionaryDemographicPopulationToLookAtYearTwo[percentofDemoInPrecinctOverTime.Key];

                  float changeOfDemoInPrecinctOverTime = popOfDemoYearTwoFloat - popofDemoYearOneFloat;

                  precinctColorChangeGameObjectFinder = "" + percentofDemoInPrecinctOverTime.Key; // Changes to string

                  if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
                   {
                     precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
                     currentPrecicntIsSplit = true;
                   }

                  else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
                   {
                     precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);

                     if(precinctColorChangeGameObject == null)
                      {
                        Debug.Log("A game object for this precinct " + percentofDemoInPrecinctOverTime.Key + " does not exist.");
                      }
                   }

                   if(precinctColorChangeGameObject != null)
                    {
                      if(changeOfDemoInPrecinctOverTime > 0) /// The pop of this demo grew over time
                        {
                          Debug.Log("Working"  + changeOfDemoInPrecinctOverTime);
                          popVisColor1 = 1 - (changeOfDemoInPrecinctOverTime / 50);
                          popVisColor2 = 1;
                          popVisColor3 = 1 - (changeOfDemoInPrecinctOverTime / 50);

                          gameObject.GetComponent<InitStartup>().demoChangeOverTimePos.Add("" + percentofDemoInPrecinctOverTime.Key, changeOfDemoInPrecinctOverTime);
                        }

                      else if(changeOfDemoInPrecinctOverTime < 0) /// The pop of this demo fell over time
                        {
                          changeOfDemoInPrecinctOverTime *= -1f;

                          popVisColor1 = 1;
                          popVisColor2 = 1 - (changeOfDemoInPrecinctOverTime / 50);
                          popVisColor3 = 1 - (changeOfDemoInPrecinctOverTime / 50);

                          gameObject.GetComponent<InitStartup>().demoChangeOverTimeNeg.Add("" + percentofDemoInPrecinctOverTime.Key, changeOfDemoInPrecinctOverTime);
                        }

                        if(currentPrecicntIsSplit == false)
                          {
                            precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color(popVisColor1, popVisColor2, popVisColor3, 1f);
                          }


                          else if(currentPrecicntIsSplit == true)
                            {
                              foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                                {
                                  splitPrecinct.GetComponent<SpriteRenderer>().color = new Color(popVisColor1, popVisColor2, popVisColor3, 1f);
                                }

                                precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                                currentPrecicntIsSplit = false;
                              }
                    }
                }
          }
      }



      public void demoVisualizePopOneYear()
        {

              foreach(DictionaryEntry percentofDemoInPrecinct in dictionaryDemographicPopulationToLookAtYearOne)
                {

                  ////////// Sets these values

                popOfDemoFloat = (float)dictionaryDemographicPopulationToLookAtYearOne[percentofDemoInPrecinct.Key];

                //Debug.Log("" + popOfDemoFloat + " this is the Male pop percantage in " + percentofDemoInPrecinct.Key + ".");

                precinctColorChangeGameObjectFinder = "" + percentofDemoInPrecinct.Key; // Changes to string

            //    Debug.Log("" + precinctColorChangeGameObjectFinder)

                if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
                 {
                   precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
                   currentPrecicntIsSplit = true;
                 }

                else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
                 {
                   precinctColorChangeGameObject = GameObject.Find(precinctColorChangeGameObjectFinder);

                   if(precinctColorChangeGameObject == null)
                    {
                      Debug.Log("A game object for this precinct " + percentofDemoInPrecinct.Key + " does not exist.");
                    }
                 }

                 if(precinctColorChangeGameObject != null) /// If there's a game object here, which means that the preinct has a game object on the map, then go through with the coloring. Otherwise it causes a crash.
                  {



                    if(gameObject.GetComponent<UIManager>().demoDataSelector != 0) //// For the demo stuff
                      {
                        if(gameObject.GetComponent<UIManager>().demoDataSelector == 1) // White, Blue
                          {
                            popVisColor1 = 1 - (popOfDemoFloat / 100);
                            popVisColor2 = 1;
                            popVisColor3 = 1 - ( popOfDemoFloat / 100);
                        //    Debug.Log(percentofDemoInPrecinct.Key + ", these are it's RGB numbers: " + popVisColor1 + ", " + popVisColor2 + ", " + popVisColor3);
                          }

                        else if(gameObject.GetComponent<UIManager>().demoDataSelector == 2) /// Black, Green
                          {
                            popVisColor1 = 1 - (popOfDemoFloat / 100);
                            popVisColor2 = 1 - ( popOfDemoFloat / 100);
                            popVisColor3 = 1;
                          //  Debug.Log(percentofDemoInPrecinct.Key + ", these are it's RGB numbers: " + popVisColor1 + ", " + popVisColor2 + ", " + popVisColor3);
                          }

                        else if(gameObject.GetComponent<UIManager>().demoDataSelector == 3) /// Asian, Purple
                          {
                            popVisColor1 = 1;
                            popVisColor2 = 1 - ( popOfDemoFloat / 100);
                            popVisColor3 = 1;
                          //  Debug.Log(percentofDemoInPrecinct.Key + ", these are it's RGB numbers: " + popVisColor1 + ", " + popVisColor2 + ", " + popVisColor3);
                          }

                        else if(gameObject.GetComponent<UIManager>().demoDataSelector == 4) /// Hispanic, Red
                          {
                            popVisColor1 = 1;
                            popVisColor2 = 1 - ( popOfDemoFloat / 100);
                            popVisColor3 = 1 - (popOfDemoFloat / 100);
                        //    Debug.Log(percentofDemoInPrecinct.Key + ", these are it's RGB numbers: " + popVisColor1 + ", " + popVisColor2 + ", " + popVisColor3);
                          }
                      }

                    else if(gameObject.GetComponent<UIManager>().genderDataSelector != 0) /// For the gender stuff
                      {
                      //  Debug.Log("Is this working at least");
                        if(gameObject.GetComponent<UIManager>().genderDataSelector == 1) // Male, Blue
                          {
                            Debug.Log("Color is working for male");
                            popVisColor1 = 1 - (popOfDemoFloat / 100);
                            popVisColor2 = 1 - ( popOfDemoFloat / 100);
                            popVisColor3 = 1;
                        //    Debug.Log(percentofDemoInPrecinct.Key + ", these are it's RGB numbers: " + popVisColor1 + ", " + popVisColor2 + ", " + popVisColor3);
                          }


                        if(gameObject.GetComponent<UIManager>().genderDataSelector == 2) // Female, Pink
                          {
                            Debug.Log("Color is working for female");
                            popVisColor1 = 1 - (popOfDemoFloat / 100);
                            popVisColor2 = 1;
                            popVisColor3 = 1 - ( popOfDemoFloat / 100);
                        //    Debug.Log(percentofDemoInPrecinct.Key + ", these are it's RGB numbers: " + popVisColor1 + ", " + popVisColor2 + ", " + popVisColor3);
                          }
                      }


                  //  Debug.Log("Currently on " + percentofDemoInPrecinct.Key + ".");


                    if(currentPrecicntIsSplit == false)
                      {
                        precinctColorChangeGameObject.GetComponent<SpriteRenderer>().color = new Color(popVisColor1, popVisColor2, popVisColor3, 1f);
                      }


                      else if(currentPrecicntIsSplit == true)
                        {
                          foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                            {
                              splitPrecinct.GetComponent<SpriteRenderer>().color = new Color(popVisColor1, popVisColor2, popVisColor3, 1f);
                            }

                            precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                            currentPrecicntIsSplit = false;
                          }
                      }
                    }
                }
        }
