using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public int layoutSelectionInteger = 1;
    public int demoInteger = 0;
    public int genderInteger = 0;
    public int specialFuncInteger = 0;

    private GameObject precinctColorChangeGameObjectButForUI;
    private GameObject[] precinctColorChangeGameObjectsForSplitPrecincts;
    private string precinctColorChangeGameObjectFinder;
    private bool currentPrecicntIsSplit;




    ///////////////////
    //// Menu stuff
    //////////////////


    //// Info displayer
    public Text dataTextBox;


    /// Data Menu

    public GameObject menuPanel;

    private bool isMenuActive = false;
    public TextMeshProUGUI openAndCloseButtonText;
    public TMP_InputField yearOneInputField;
    public string yearOneString;
    public TMP_InputField yearTwoInputField;
    public string yearTwoString;

    /// Demograhpic Picker

    public int demoDataSelector = 0;
    public bool demoActivated;

    public int genderDataSelector = 0;
    public bool genderActivated;

    public int specialFuncSelector = 0;
    public bool specialFuncActivated;

    public bool doingTwoYears = false;
    public bool doingOneYear = false;



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////
//// BEGIN FUNCTIONS
////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void selectYearOne ()
      {
        yearOneString = yearOneInputField.text; /// Turns the text into a sting

        Debug.Log(yearOneString); /// Just for testing purposes


        if(yearOneString == "2012") //// Depending on which year is selected, loads in those hashtables
          {
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanYearOne = gameObject.GetComponent<InitStartup>().republicanVotes2012;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticYearOne = gameObject.GetComponent<InitStartup>().democratVotes2012;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersYearOne = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2012;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsYearOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2012;
          }


        if(yearOneString == "2016")
          {
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanYearOne = gameObject.GetComponent<InitStartup>().republicanVotes2016;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticYearOne = gameObject.GetComponent<InitStartup>().democratVotes2016;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersYearOne = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2016;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsYearOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2016;
          }


        if(yearOneString == "2018")
          {
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanYearOne = gameObject.GetComponent<InitStartup>().republicanVotes2018;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticYearOne = gameObject.GetComponent<InitStartup>().democratVotes2018;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersYearOne = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2018;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsYearOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2018;
          }


        if(yearOneString == "2020")
          {
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanYearOne = gameObject.GetComponent<InitStartup>().republicanVotes2020;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticYearOne = gameObject.GetComponent<InitStartup>().democratVotes2020;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersYearOne = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2020;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsYearOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2020;
          }

        if(yearOneString == "2021")
          {
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanYearOne = gameObject.GetComponent<InitStartup>().republicanVotes2021;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticYearOne = gameObject.GetComponent<InitStartup>().democratVotes2021;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersYearOne = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2021;
            gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsYearOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2021;
          }
      }



      public void selectYearTwo ()
        {
          yearTwoString = yearTwoInputField.text; /// Turns the text into a sting

          Debug.Log(yearTwoString); /// Just for testing purposes


          if(yearTwoString == "2012") //// Depending on which year is selected, loads in those hashtables
            {
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanYearTwo = gameObject.GetComponent<InitStartup>().republicanVotes2012;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticYearTwo = gameObject.GetComponent<InitStartup>().democratVotes2012;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersYearTwo = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2012;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsYearTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2012;
            }


          if(yearTwoString == "2016")
            {
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanYearTwo = gameObject.GetComponent<InitStartup>().republicanVotes2016;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticYearTwo = gameObject.GetComponent<InitStartup>().democratVotes2016;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersYearTwo = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2016;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsYearTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2016;
            }


          if(yearTwoString == "2018")
            {
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanYearTwo = gameObject.GetComponent<InitStartup>().republicanVotes2018;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticYearTwo = gameObject.GetComponent<InitStartup>().democratVotes2018;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersYearTwo = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2018;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsYearTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2018;
            }


          if(yearTwoString == "2020")
            {
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanYearTwo = gameObject.GetComponent<InitStartup>().republicanVotes2020;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticYearTwo = gameObject.GetComponent<InitStartup>().democratVotes2020;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersYearTwo = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2020;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsYearTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2020;
            }

          if(yearTwoString == "2021")
            {
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanYearTwo = gameObject.GetComponent<InitStartup>().republicanVotes2021;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticYearTwo = gameObject.GetComponent<InitStartup>().democratVotes2021;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersYearTwo = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2021;
              gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsYearTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2021;
            }
        }


/////////////////////////////////////////////////////////////////////////
/// GENDER STUFF
/////////////////////////////////////////////////////////////////////////

  public void selectedGender(int genderInteger)
    {

      if(genderInteger != 0)
        {
          if(genderActivated != true)
            {
              Debug.Log("Turned on Gender");
              genderActivated = true;
            }
        }

      if(genderInteger == 0 && genderActivated == true)
        {
          Debug.Log("Turned Off Gender");
          genderActivated = false;
        }


      if(genderInteger == 1)
        {
          Debug.Log("Selected Male");
          genderDataSelector = 1;
        }

      if(genderInteger == 2)
        {
          Debug.Log("Selected Female");
          genderDataSelector = 2;
        }
    }



/////////////////////////////////////////////////////////////////////////
/// DEMOGRAPHIC STUFF
/////////////////////////////////////////////////////////////////////////


  public void selectDemographic(int demoInteger)
    {

      if(demoInteger != 0)
        {
          if(demoActivated != true)
            {
              demoActivated = true;
              Debug.Log("Turned on Demo");
            }
        }

      if(demoInteger == 0 && demoActivated == true)
        {
          demoActivated = false;
          Debug.Log("Turned off Demo");
        }

      if(demoInteger == 1)
        {
          Debug.Log("Selected White Demo");

          demoDataSelector = 1;
        }

      if(demoInteger == 2)
        {
          Debug.Log("Selected Black Demo");

          demoDataSelector = 2;
        }

      if(demoInteger == 3)
        {
          Debug.Log("Selected Asian Demo");

          demoDataSelector = 3;
        }

      if(demoInteger == 4)
        {
          Debug.Log("Selected Latino Demo");

          demoDataSelector = 4;
        }

      if(demoInteger == 5)
        {
          Debug.Log("Selected Majority");

          demoDataSelector = 5;
        }
    }


    /////////////////////////////////////////////////////////////////////////
    /// DEMOGRAPHIC STUFF
    /////////////////////////////////////////////////////////////////////////


      public void selectSpecialFunction(int specialFuncInteger)
        {

          if(specialFuncInteger != 0)
            {
              if(specialFuncActivated != true)
                {
                  specialFuncActivated = true;
                  Debug.Log("Turned on Special Func");
                }
            }

          if(specialFuncInteger == 0 && specialFuncActivated == true)
            {
              specialFuncActivated = false;
              Debug.Log("Turned off Special Func");
            }

          if(specialFuncInteger == 1)
            {
              Debug.Log("Doing PVI");
              specialFuncSelector = 1;
            }

          if(specialFuncInteger == 2)
            {
              Debug.Log("Doing PVI Shift");
              specialFuncSelector = 2;
            }

          if(specialFuncInteger == 3) /// Flipped Dem
            {
              Debug.Log("Selected Flipped Dem");

              specialFuncSelector = 3;
            }

          if(specialFuncInteger == 4) /// Flipped Rep
            {
              Debug.Log("Selected Turnout");

              specialFuncSelector = 4;
            }

            if(specialFuncInteger == 5) /// Ballots
              {
                Debug.Log("Selected Casted Ballots");

                specialFuncSelector = 5;
              }
        }


/////////////////////////////////////////////////////
/// SHOWING RESULTS
////////////////////////////////////////////////////

    public void showResults ()
      {
        resetData(); /// Resets all the data tables so multiple simulations can be run

        if(yearTwoString == null || yearTwoString == "") /// One year Layout
          {

            Debug.Log("Doing One Year Layout");

            doingOneYear = true;

            if(demoActivated == true) /// One year layout with Demograhpics
              {
                pickDemographicAndGender();
                gameObject.GetComponent<DemographicManager>().demoVisualizePopOneYear();
              }

            if(specialFuncActivated == true) /// One year layout with special functions
              {
                Debug.Log("Stating Special Functions");
                pickSpecialFunction();

                if(specialFuncSelector == 1)
                  {
                    gameObject.GetComponent<ElectionCycleManager>().implimentPVICalculations();
                  }

                if(specialFuncSelector == 4)
                  {
                    gameObject.GetComponent<ElectionCycleManager>().implimentTurnoutVisualization();
                  }
              }

            else if(demoActivated == false && specialFuncActivated == false) /// One year Layout without Demo
              {
                gameObject.GetComponent<ElectionCycleManager>().implimentElectionLayoutOneCycle();
              }
          }

        else if(yearTwoString != null || yearTwoString != "")
          {

            Debug.Log("Doing Two Year Layout");

            doingTwoYears = true;

            if(demoActivated == true) /// One year layout with Demograhpics
              {
                Debug.Log("Starting Two Year Demo");
                pickDemographicAndGender();
                Debug.Log("Go Two Year Demo!");
                gameObject.GetComponent<DemographicManager>().demoVisualizePopTwoYears();
              }

            if(specialFuncActivated == true) /// One year layout with special functions
              {
                pickSpecialFunction();

                Debug.Log("Pre check");
                if(specialFuncSelector == 2)
                  {
                    Debug.Log("Doing 2 year PVI");
                    gameObject.GetComponent<ElectionCycleManager>().implimentPVICalcsOverTwoYears();
                  }

                if(specialFuncSelector == 4)
                  {
                    Debug.Log("it's a go!");
                    gameObject.GetComponent<ElectionCycleManager>().implimentTurnoutVisualizationTwoYears();
                  }

                  if(specialFuncSelector == 5)
                    {
                      Debug.Log("it's a go!");
                      gameObject.GetComponent<ElectionCycleManager>().implimentCastedBallotsVisualization();
                    }
              }

              else if(demoActivated == false && specialFuncActivated == false) /// One year Layout without Demo
                {
                  Debug.Log("Begining Year Two Cycle Layout");
                  gameObject.GetComponent<ElectionCycleManager>().implimentElectionLayoutTwocycle();
                }
          }
        }

    //////////////////////////////////////
    /// RESETING DATA STUFF
    //////////////////////////////////////


    public void pickDemographicAndGender()
      {

        if(yearOneString == "2012")
          {

            if(demoDataSelector == 1)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2012;
              }

            else if(demoDataSelector == 2)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2012;
              }

            else if(demoDataSelector == 3)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2012;
              }

            else if(demoDataSelector == 4)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2012;
              }
          }


        else if(yearOneString == "2016")
          {

            if(demoDataSelector == 1)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2016;
              }

            else if(demoDataSelector == 2)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2016;
              }

            else if(demoDataSelector == 3)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2016;
              }

            else if(demoDataSelector == 4)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2016;
              }
          }


        else if(yearOneString == "2018")
          {

            if(demoDataSelector == 1)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2018;
              }

            else if(demoDataSelector == 2)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2018;
              }

            else if(demoDataSelector == 3)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2018;
              }

            else if(demoDataSelector == 4)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2018;
              }
          }

        else if(yearOneString == "2020")
          {

            if(demoDataSelector == 1)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2020;
              }

            else if(demoDataSelector == 2)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2020;
              }

            else if(demoDataSelector == 3)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2020;
              }

            else if(demoDataSelector == 4)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2020;
              }
          }


        else if(yearOneString == "2021")
          {

            if(demoDataSelector == 1)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2021;
              }

            else if(demoDataSelector == 2)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2021;
              }

            else if(demoDataSelector == 3)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2021;
              }

            else if(demoDataSelector == 4)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearOne = gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2021;
              }
          }

          ////////////////////////////////////////
          /// Year Two
          ////////////////////////////////////////

        if(yearTwoString == "2012")
          {

            if(demoDataSelector == 1)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2012;
              }

            else if(demoDataSelector == 2)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2012;
              }

            else if(demoDataSelector == 3)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2012;
              }

            else if(demoDataSelector == 4)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2012;
              }
          }


        else if(yearTwoString == "2016")
          {

            if(demoDataSelector == 1)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2016;
              }

            else if(demoDataSelector == 2)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2016;
              }

            else if(demoDataSelector == 3)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2016;
              }

            else if(demoDataSelector == 4)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2016;
              }
          }


        else if(yearTwoString == "2018")
          {

            if(demoDataSelector == 1)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2018;
              }

            else if(demoDataSelector == 2)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2018;
              }

            else if(demoDataSelector == 3)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2018;
              }

            else if(demoDataSelector == 4)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2018;
              }
          }

        else if(yearTwoString == "2020")
          {

            if(demoDataSelector == 1)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2020;
              }

            else if(demoDataSelector == 2)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2020;
              }

            else if(demoDataSelector == 3)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2020;
              }

            else if(demoDataSelector == 4)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2020;
              }
          }


        else if(yearTwoString == "2021")
          {

            if(demoDataSelector == 1)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().whitePopulationOfPrecinct2021;
              }

            else if(demoDataSelector == 2)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().blackPopulationOfPrecinct2021;
              }

            else if(demoDataSelector == 3)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().asianPopulationOfPrecinct2021;
              }

            else if(demoDataSelector == 4)
              {
                gameObject.GetComponent<DemographicManager>().dictionaryDemographicPopulationToLookAtYearTwo = gameObject.GetComponent<InitStartup>().hispanicPopulationOfPrecinct2021;
              }
          }
      }

     public void pickSpecialFunction()
      {
        if(specialFuncSelector == 3) /// Flipped Precincts
          {
            gameObject.GetComponent<ElectionCycleManager>().implimentFlipsLayoutTwoCycles();
          }

        if(specialFuncSelector == 5)
          {
            Debug.Log("Doing Ballots");

            if(yearTwoString == "2012")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2012;
              }

            if(yearTwoString == "2016")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2016;
              }

            if(yearTwoString == "2018")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2018;
              }

            if(yearTwoString == "2020")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2020;
              }


            if(yearTwoString == "2021")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2021;
              }


            if(yearOneString == "2012")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2012;
              }

            if(yearOneString == "2016")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2016;
              }

            if(yearOneString == "2018")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2018;
              }

            if(yearOneString == "2020")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2020;
              }


            if(yearOneString == "2021")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2021;
              }
          }


        if(specialFuncSelector == 4) /// Turnout
          {
            Debug.Log("Doing Turnout");
            if(yearTwoString == "2012")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2012;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersTwo = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2012;
              }

            if(yearTwoString == "2016")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2016;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersTwo = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2016;
              }

            if(yearTwoString == "2018")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2018;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersTwo = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2018;
              }

            if(yearTwoString == "2020")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2020;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersTwo = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2020;
              }


            if(yearTwoString == "2021")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsTwo = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2021;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersTwo = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2021;
              }







            if(yearOneString == "2012")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2012;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersOne = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2012;
              }

            if(yearOneString == "2016")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2016;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersOne = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2016;
              }

            if(yearOneString == "2018")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2018;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersOne = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2018;
              }

            if(yearOneString == "2020")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2020;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersOne = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2020;
              }


            if(yearOneString == "2021")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtCastedBallotsOne = gameObject.GetComponent<InitStartup>().castedBallotsInPrecinct2021;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRegisteredVotersOne = gameObject.GetComponent<InitStartup>().registeredVotersInPrecinct2021;
              }
          }

         else if(specialFuncSelector == 1 || specialFuncSelector == 2)
          {
            //Debug.Log("is this working at least?");


            if(yearOneString == "2016")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticPVI = gameObject.GetComponent<InitStartup>().democraticPVIfor2016;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanPVI = gameObject.GetComponent<InitStartup>().republicanPVIfor2016;

              }


            else if(yearOneString == "2018")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticPVI = gameObject.GetComponent<InitStartup>().democraticPVIfor2018;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanPVI = gameObject.GetComponent<InitStartup>().republicanPVIfor2018;

              }

            else if(yearOneString == "2020")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticPVI = gameObject.GetComponent<InitStartup>().democraticPVIfor2020;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanPVI = gameObject.GetComponent<InitStartup>().republicanPVIfor2020;

              }


            else if(yearOneString == "2021")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticPVI = gameObject.GetComponent<InitStartup>().democraticPVIfor2021;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanPVI = gameObject.GetComponent<InitStartup>().republicanPVIfor2021;

              }



            if(yearTwoString == "2016")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticPVITwo = gameObject.GetComponent<InitStartup>().democraticPVIfor2016;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanPVITwo = gameObject.GetComponent<InitStartup>().republicanPVIfor2016;

              }


            else if(yearTwoString == "2018")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticPVITwo = gameObject.GetComponent<InitStartup>().democraticPVIfor2018;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanPVITwo = gameObject.GetComponent<InitStartup>().republicanPVIfor2018;

              }

            else if(yearTwoString == "2020")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticPVITwo = gameObject.GetComponent<InitStartup>().democraticPVIfor2020;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanPVITwo = gameObject.GetComponent<InitStartup>().republicanPVIfor2020;

              }


            else if(yearTwoString == "2021")
              {
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtDemocraticPVITwo = gameObject.GetComponent<InitStartup>().democraticPVIfor2021;
                gameObject.GetComponent<ElectionCycleManager>().dictionaryYearToLookAtRepublicanPVITwo = gameObject.GetComponent<InitStartup>().republicanPVIfor2021;
              }
          }
      }
    public void resetData()
      {
        ///// Clears the display hashtables

        gameObject.GetComponent<UIManager>().dataTextBox.text = "";

        gameObject.GetComponent<InitStartup>().turnoutPercentage.Clear(); //
        gameObject.GetComponent<InitStartup>().castedBallots.Clear(); //
        gameObject.GetComponent<InitStartup>().marginOfWinRepublicanWin.Clear(); //
        gameObject.GetComponent<InitStartup>().marginOfWinDemocratWin.Clear(); //
        gameObject.GetComponent<InitStartup>().percentageDemocratic.Clear(); //
        gameObject.GetComponent<InitStartup>().percentageRepublican.Clear(); //
        gameObject.GetComponent<InitStartup>().tiedPrecinct.Clear(); //
        gameObject.GetComponent<InitStartup>().demoChangeOverTimeNeg.Clear();
        gameObject.GetComponent<InitStartup>().demoChangeOverTimePos.Clear();
        gameObject.GetComponent<InitStartup>().republicanPVI.Clear(); //
        gameObject.GetComponent<InitStartup>().democraticPVI.Clear(); //
        gameObject.GetComponent<InitStartup>().changeInMarginRepublican.Clear(); //
        gameObject.GetComponent<InitStartup>().changeInMarginDemocratic.Clear(); //
        gameObject.GetComponent<InitStartup>().changeInCastedBallots.Clear();
        gameObject.GetComponent<InitStartup>().changeInTurnout.Clear(); //
        gameObject.GetComponent<InitStartup>().changeInMarginBoth.Clear();
        gameObject.GetComponent<InitStartup>().changeInCastedBallots.Clear(); //
        gameObject.GetComponent<InitStartup>().changeinCastedBalPercentageTable.Clear(); //
        gameObject.GetComponent<InitStartup>().changeInRepublicanPVINeg.Clear(); //
        gameObject.GetComponent<InitStartup>().changeInDemocraticPVINeg.Clear(); //
        gameObject.GetComponent<InitStartup>().changeInPVIDemFlip.Clear(); //
        gameObject.GetComponent<InitStartup>().changeInPVIRepFlip.Clear(); //
        gameObject.GetComponent<InitStartup>().changeInRepublicanPVI.Clear(); //
        gameObject.GetComponent<InitStartup>().changeInDemocraticPVI.Clear(); //
        gameObject.GetComponent<InitStartup>().demoChangeOverTimeNeg.Clear();
        gameObject.GetComponent<InitStartup>().demoChangeOverTimePos.Clear();
        doingOneYear = false;
        doingTwoYears = false;

        foreach(DictionaryEntry resetPrecinct in gameObject.GetComponent<InitStartup>().republicanVotes2018)
          {
            precinctColorChangeGameObjectFinder = "" + resetPrecinct.Key;

            if(gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// If one of the multiple objected precincts, find the tag instead
            {
              precinctColorChangeGameObjectsForSplitPrecincts = GameObject.FindGameObjectsWithTag(precinctColorChangeGameObjectFinder); /// Finds the GO tagged with the precinct name for split ones
              currentPrecicntIsSplit = true;
            }

            else if (!gameObject.GetComponent<InitStartup>().listOfSplitPrecincts.Contains(precinctColorChangeGameObjectFinder)) /// otherwise, just do what usually happens
            {
              //  Debug.Log(precinctColorChangeGameObjectFinder);
              precinctColorChangeGameObjectButForUI = GameObject.Find(precinctColorChangeGameObjectFinder);
            }

            if(precinctColorChangeGameObjectButForUI != null)
              {

                Debug.Log("Currently making white "  + resetPrecinct.Key);

                
                if(currentPrecicntIsSplit == false)
                  {
                    precinctColorChangeGameObjectButForUI.GetComponent<SpriteRenderer>().color = new Color (1, 1, 1, 1);
                  }

                if(currentPrecicntIsSplit == true)
                  {
                    foreach(GameObject splitPrecinct in precinctColorChangeGameObjectsForSplitPrecincts)
                      {
                        splitPrecinct.GetComponent<SpriteRenderer>().color = new Color (1, 1, 1, 1);
                      }

                      precinctColorChangeGameObjectsForSplitPrecincts = new GameObject[0];
                      currentPrecicntIsSplit = false;
                  }
              }
          }
        }


    ////////////////////////////////////////////////
    ///// MENU OPEN/CLOSE STUFF
    ////////////////////////////////////////////////

    public void menuActiveDeterminer ()
      {
        if(isMenuActive == false) /// If the menu isn't open, then open it
          {
            openMenu();
          }

         else if(isMenuActive == true) /// If the menu IS open, close it
          {
            closeMenu();
          }
      }

    public void openMenu ()
      {

        Debug.Log("Yeah the menu is opened great job!");

        openAndCloseButtonText.text = "Close Menu"; /// Changes the text of the button

        menuPanel.SetActive(true); /// Sets the menu active

        isMenuActive = true; /// Does this so the button will act as a close button now


      }

    public void closeMenu()
      {
        Debug.Log("Yeah the menu is closed great job!");

        openAndCloseButtonText.text = "Open Menu"; /// Changes the text of the button

        menuPanel.SetActive(false); /// Makes the menu unactive

        isMenuActive = false; /// Does this so the button will act as an open button now


      }
    }
