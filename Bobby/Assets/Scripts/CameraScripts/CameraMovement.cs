using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{


  // WOW THIS SHIT ACTUALLY WORKS!!!

  public int CamMovementSpeed = 5;

  public float CamZoom = 0.05f;

  public float CamZoomScrollWheel = 1f;

  public int MinimumCamHeight = 3;

  public int MaximumCamHeight = 24;

  public Camera main_Cam;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

      // Up/Down on the Y axis Scroll Wheel

      var MouseWheelInput = Input.GetAxis("Mouse ScrollWheel");

      if(MouseWheelInput < 0f && main_Cam.orthographicSize < MaximumCamHeight)
        {
          var CurrentCamOrthoSize = main_Cam.orthographicSize;

          main_Cam.orthographicSize = CurrentCamOrthoSize + CamZoomScrollWheel;
        }

      if(MouseWheelInput > 0f && main_Cam.orthographicSize > MinimumCamHeight)
        {
          var CurrentCamOrthoSize = main_Cam.orthographicSize;

          main_Cam.orthographicSize = CurrentCamOrthoSize - CamZoomScrollWheel;
        }

      // Up/Down on the Y axis Keys

      if(Input.GetKey(KeyCode.E) && main_Cam.orthographicSize > MinimumCamHeight)
        {
          var CurrentCamOrthoSize = main_Cam.orthographicSize;

          main_Cam.orthographicSize = CurrentCamOrthoSize - CamZoom;
        }

      if(Input.GetKey(KeyCode.Q) && main_Cam.orthographicSize < MaximumCamHeight)
        {
          var CurrentCamOrthoSize = main_Cam.orthographicSize;

          main_Cam.orthographicSize = CurrentCamOrthoSize + CamZoom;
        }

      // Side/Side

      if(Input.GetKey(KeyCode.D))
        {
        transform.Translate(new Vector3(CamMovementSpeed * Time.deltaTime,0,0));
        }

      if(Input.GetKey(KeyCode.A))
        {
        transform.Translate(new Vector3(-CamMovementSpeed * Time.deltaTime,0,0));
        }

      // Up/Down on the X axis

      if(Input.GetKey(KeyCode.W))
        {
        transform.Translate(new Vector3(0, CamMovementSpeed * Time.deltaTime, 0));
        }

      if(Input.GetKey(KeyCode.S))
        {
        transform.Translate(new Vector3(0, -CamMovementSpeed * Time.deltaTime, 0));
        }

    }
}
